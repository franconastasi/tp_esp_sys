#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  8 12:58:49 2018

@author: franco
"""

import scipy.io.wavfile as wfile

import matplotlib.pyplot as plt

import numpy as np

wavFile = "hh15.WAV"

wavFileMasc = "hh15Masc.WAV" #archivo grabado por voz masculina

sampleRate, data  = wfile.read(wavFile)
t = np.arange(len(data))/sampleRate


sampleRateMasc, dataMasc  = wfile.read(wavFileMasc)
t_masc = np.arange(len(dataMasc))/sampleRateMasc



fig, (ax1, ax2) = plt.subplots(2,1)
ax1.plot(t, data, label="data")
ax2.plot(t_masc, dataMasc, label="dataMasc")

ax1.set(xlabel= "Tiempo [s]", ylabel = "Amplitud", title = "Señal muestreada original")

ax2.set(xlabel= "Tiempo [s]", ylabel = "Amplitud", title = "Señal muestreada - voz masculina")
#ax2.set_ylim(bottom= 0.5, top = 0.75)

y_ticks = np.arange(-10000,25001,5000) # Seteo ticks para tener un grid con más definición 

ax1.set_yticks(y_ticks)
#ax2.set_yticks(y_ticks)

ax1.grid()
ax2.grid()

fig.tight_layout()

fig.savefig("comparación_voz", dpi = 300)
