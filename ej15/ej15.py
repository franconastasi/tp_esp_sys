#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 24 00:42:03 2018

@author: franco
"""


import scipy.io.wavfile as wfile

import matplotlib.pyplot as plt

import numpy as np

from scipy import signal as sg

#zpk_filter = np.load("zpk.npy")
Fs =16e3
Nfs = Fs/2
F1 = 150
F11 = F1/2
F2 = 250
F22 = F2/2

t_50ms_16kHz = np.arange(0,0.05,1/Fs)
# Genero un filtro bandstop de orden 4, analógico con frecuencias de corte en 100Hz y 300 Hz 





# Codigo para ver la respuesta en frecuencia 
w, h = sg.freqz(b, a)

plt.plot((Fs * 0.5 / np.pi) * w,(abs(h)))
plt.xscale('log')



b, a = sg.butter(4, [F1/Nfs, F2/Nfs], 'bandstop', analog=False)


###################################################################
######################## FONO a          ##########################
###################################################################



fono_a_sint_filto_bs = sg.lfilter(b,a,abs(fono_a_sint))

fig_fono_a_filtro, ax_fono_a_filtro =plt.subplots(2,1)
fig_fono_a_filtro.text(0.5,0.01,'Tiempo [ms]',ha='center')
fig_fono_a_filtro.text(0.02,0.5,'Amplitud', rotation='vertical')

ax_fono_a_filtro[0].plot(t_50ms_16kHz*1e3,fono_a_sint)
ax_fono_a_filtro[0].set(title="Fono [a] sintetizado")
ax_fono_a_filtro[1].plot(t_50ms_16kHz*1e3,fono_a_sint_filto_bs)
ax_fono_a_filtro[1].set( title="Fono [a] sintetizado pasado por un filtro 'bandstop'")

fig_fono_a_filtro.tight_layout()
fig_fono_a_filtro.savefig("fig_fono_a_filtro",dpi=300)


fig_fono_a_filtro_freq, ax_fono_a_filtro_freq =plt.subplots(2,1)

fig_fono_a_filtro_freq.text(0.5,0.01,'Frecuencia [Hz]',ha='center')
fig_fono_a_filtro_freq.text(0.02,0.55,'Espectro', rotation='vertical')

ax_fono_a_filtro_freq[0].plot(Fs* (np.arange(0,1,1/len(fono_a_sint)))[0:50] , abs(np.fft.fft(abs(fono_a_sint)))[0:50])
ax_fono_a_filtro_freq[0].set(title="Espectro del fono [a] sintetizado")
ax_fono_a_filtro_freq[1].plot(Fs* (np.arange(0,1,1/len(fono_a_sint_filto_bs)))[0:50] , abs(np.fft.fft(abs(fono_a_sint_filto_bs)))[0:50])
ax_fono_a_filtro_freq[1].set(title="Espectro del fono [a] sintetizado luego del filtro")

fig_fono_a_filtro_freq.tight_layout()
fig_fono_a_filtro_freq.savefig("fig_fono_a_filtro_freq",dpi=300)


###################################################################
###################################################################
###################################################################
###################################################################






###################################################################
######################## FONO e          ##########################
###################################################################



fono_e_sint_filto_bs = sg.lfilter(b,a,abs(fono_e_sint))

fig_fono_e_filtro, ax_fono_e_filtro =plt.subplots(2,1)
fig_fono_e_filtro.text(0.5,0.01,'Tiempo [ms]',ha='center')
fig_fono_e_filtro.text(0.02,0.5,'Amplitud', rotation='vertical')

ax_fono_e_filtro[0].plot(t_50ms_16kHz*1e3,fono_e_sint)
ax_fono_e_filtro[0].set(title="Fono [e] sintetizado")
ax_fono_e_filtro[1].plot(t_50ms_16kHz*1e3,fono_e_sint_filto_bs)
ax_fono_e_filtro[1].set( title="Fono [e] sintetizado pasado por un filtro 'bandstop'")

fig_fono_e_filtro.tight_layout()
fig_fono_e_filtro.savefig("fig_fono_e_filtro",dpi=300)


fig_fono_e_filtro_freq, ax_fono_e_filtro_freq =plt.subplots(2,1)

fig_fono_e_filtro_freq.text(0.5,0.01,'Frecuencia [Hz]',ha='center')
fig_fono_e_filtro_freq.text(0.02,0.55,'Espectro', rotation='vertical')

ax_fono_e_filtro_freq[0].plot(Fs* (np.arange(0,1,1/len(fono_e_sint)))[0:50] , abs(np.fft.fft(abs(fono_e_sint)))[0:50])
ax_fono_e_filtro_freq[0].set(title="Espectro del fono [e] sintetizado")
ax_fono_e_filtro_freq[1].plot(Fs* (np.arange(0,1,1/len(fono_e_sint_filto_bs)))[0:50] , abs(np.fft.fft(abs(fono_e_sint_filto_bs)))[0:50])
ax_fono_e_filtro_freq[1].set(title="Espectro del fono [e] sintetizado luego del filtro")

fig_fono_e_filtro_freq.tight_layout()
fig_fono_e_filtro_freq.savefig("fig_fono_e_filtro_freq",dpi=300)


###################################################################
###################################################################
###################################################################
###################################################################



###################################################################
######################## FONO i        ##########################
###################################################################



fono_i_sint_filto_bs = sg.lfilter(b,a,abs(fono_i_sint))

fig_fono_i_filtro, ax_fono_i_filtro =plt.subplots(2,1)
fig_fono_i_filtro.text(0.5,0.01,'Tiempo [ms]',ha='center')
fig_fono_i_filtro.text(0.02,0.5,'Amplitud', rotation='vertical')

ax_fono_i_filtro[0].plot(t_50ms_16kHz*1e3,fono_i_sint)
ax_fono_i_filtro[0].set(title="Fono [i] sintetizado")
ax_fono_i_filtro[1].plot(t_50ms_16kHz*1e3,fono_i_sint_filto_bs)
ax_fono_i_filtro[1].set( title="Fono [i] sintetizado pasado por un filtro 'bandstop'")

fig_fono_i_filtro.tight_layout()
fig_fono_i_filtro.savefig("fig_fono_i_filtro",dpi=300)


fig_fono_i_filtro_freq, ax_fono_i_filtro_freq =plt.subplots(2,1)

fig_fono_i_filtro_freq.text(0.5,0.01,'Frecuencia [Hz]',ha='center')
fig_fono_i_filtro_freq.text(0.02,0.55,'Espectro', rotation='vertical')

ax_fono_i_filtro_freq[0].plot(Fs* (np.arange(0,1,1/len(fono_i_sint)))[0:50] , abs(np.fft.fft(abs(fono_i_sint)))[0:50])
ax_fono_i_filtro_freq[0].set(title="Espectro del fono [i] sintetizado")
ax_fono_i_filtro_freq[1].plot(Fs* (np.arange(0,1,1/len(fono_i_sint_filto_bs)))[0:50] , abs(np.fft.fft(abs(fono_i_sint_filto_bs)))[0:50])
ax_fono_i_filtro_freq[1].set(title="Espectro del fono [i] sintetizado luego del filtro")

fig_fono_i_filtro_freq.tight_layout()
fig_fono_i_filtro_freq.savefig("fig_fono_i_filtro_freq",dpi=300)


###################################################################
###################################################################
###################################################################
###################################################################




###################################################################
######################## FONO o        ##########################
###################################################################



fono_o_sint_filto_bs = sg.lfilter(b,a,abs(fono_o_sint))

fig_fono_o_filtro, ax_fono_o_filtro =plt.subplots(2,1)
fig_fono_o_filtro.text(0.5,0.01,'Tiempo [ms]',ha='center')
fig_fono_o_filtro.text(0.02,0.5,'Amplitud', rotation='vertical')

ax_fono_o_filtro[0].plot(t_50ms_16kHz*1e3,fono_o_sint)
ax_fono_o_filtro[0].set(title="Fono [o] sintetizado")
ax_fono_o_filtro[1].plot(t_50ms_16kHz*1e3,fono_o_sint_filto_bs)
ax_fono_o_filtro[1].set( title="Fono [o] sintetizado pasado por un filtro 'bandstop'")

fig_fono_o_filtro.tight_layout()
fig_fono_o_filtro.savefig("fig_fono_o_filtro",dpi=300)


fig_fono_o_filtro_freq, ax_fono_o_filtro_freq =plt.subplots(2,1)

fig_fono_o_filtro_freq.text(0.5,0.01,'Frecuencia [Hz]',ha='center')
fig_fono_o_filtro_freq.text(0.02,0.55,'Espectro', rotation='vertical')

ax_fono_o_filtro_freq[0].plot(Fs* (np.arange(0,1,1/len(fono_o_sint)))[0:50] , abs(np.fft.fft(abs(fono_o_sint)))[0:50])
ax_fono_o_filtro_freq[0].set(title="Espectro del fono [o] sintetizado")
ax_fono_o_filtro_freq[1].plot(Fs* (np.arange(0,1,1/len(fono_o_sint_filto_bs)))[0:50] , abs(np.fft.fft(abs(fono_o_sint_filto_bs)))[0:50])
ax_fono_o_filtro_freq[1].set(title="Espectro del fono [o] sintetizado luego del filtro")

fig_fono_o_filtro_freq.tight_layout()
fig_fono_o_filtro_freq.savefig("fig_fono_o_filtro_freq",dpi=300)


###################################################################
###################################################################
###################################################################
###################################################################




###################################################################
######################## FONO u        ##########################
###################################################################



fono_u_sint_filto_bs = sg.lfilter(b,a,abs(fono_o_sint))

fig_fono_u_filtro, ax_fono_u_filtro =plt.subplots(2,1)
fig_fono_u_filtro.text(0.5,0.01,'Tiempo [ms]',ha='center')
fig_fono_u_filtro.text(0.02,0.5,'Amplitud', rotation='vertical')

ax_fono_u_filtro[0].plot(t_50ms_16kHz*1e3,fono_u_sint)
ax_fono_u_filtro[0].set(title="Fono [u] sintetizado")
ax_fono_u_filtro[1].plot(t_50ms_16kHz*1e3,fono_u_sint_filto_bs)
ax_fono_u_filtro[1].set( title="Fono [u] sintetizado pasado por un filtro 'bandstop'")

fig_fono_u_filtro.tight_layout()
fig_fono_u_filtro.savefig("fig_fono_u_filtro",dpi=300)


fig_fono_u_filtro_freq, ax_fono_u_filtro_freq =plt.subplots(2,1)

fig_fono_u_filtro_freq.text(0.5,0.01,'Frecuencia [Hz]',ha='center')
fig_fono_u_filtro_freq.text(0.02,0.55,'Espectro', rotation='vertical')

ax_fono_u_filtro_freq[0].plot(Fs* (np.arange(0,1,1/len(fono_u_sint)))[0:50] , abs(np.fft.fft(abs(fono_u_sint)))[0:50])
ax_fono_u_filtro_freq[0].set(title="Espectro del fono [u] sintetizado")
ax_fono_u_filtro_freq[1].plot(Fs* (np.arange(0,1,1/len(fono_u_sint_filto_bs)))[0:50] , abs(np.fft.fft(abs(fono_u_sint_filto_bs)))[0:50])
ax_fono_u_filtro_freq[1].set(title="Espectro del fono [u] sintetizado luego del filtro")

fig_fono_u_filtro_freq.tight_layout()
fig_fono_u_filtro_freq.savefig("fig_fono_u_filtro_freq",dpi=300)


###################################################################
###################################################################
###################################################################
###################################################################










