#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 10:09:22 2018

@author: franco
"""

import matplotlib.pyplot as plt

from scipy import signal as sg

import numpy as np

import scipy.io.wavfile as wfile


### PARA QUE SE EJECUTE BIEN, SE DEBE EJECUTAR EL EJ6 y EJ7 BIS
### EN LA MISMA CONSOLA

wavFile = "hh15.WAV"

sampleRate, data  = wfile.read(wavFile)

#Pruebas para escribir wave file
#wfile.write("WAV_Prueba.wav",int(sampleRate/2),data)


## Multiplico el tren de pulsos glóticos con el filtro y le aplico ifft
fono_a_sint = np.fft.ifft( np.multiply(tren_pulsos_fft,H_a_resp_freq[1])  )
#Lo hago periodico para que dure 1 segundo y lo guardo
fono_a_sint_1seg =abs( np.transpose(np.matlib.repmat(fono_a_sint,1,20) ))# le aplico el módulo porque son valores complejos con parte imaginario de aprox e-14
#plt.plot(fono_a_sint_1seg)
fig_a_sint, ax_a_sint = plt.subplots()
f_a_sint, t_a_sint, Sxx_a_sint = sg.spectrogram( fono_a_sint_1seg.reshape((16000,)), sampleRate, window = sg.get_window("hamming", 250))
ax_a_sint.pcolormesh(t_a_sint*1e3, f_a_sint, 10*np.log10(Sxx_a_sint),cmap = plt.get_cmap('jet'))
ax_a_sint.set_ylabel('Frecuencia [Hz]')
ax_a_sint.set_xlabel('Tiempo [ms]')
ax_a_sint.set_title("Espectrograma fono [a] sintetizado")

fig_a_sint.savefig("espect_fono_a_sint",dpi=300)

wfile.write("fono_a",sampleRate,fono_a_sint_1seg) 

########################################################################
## Multiplico el tren de pulsos glóticos con el filtro y le aplico ifft
########################################################################
fono_e_sint = np.fft.ifft(  np.multiply(tren_pulsos_fft,H_e_resp_freq[1])  )
#Lo hago periodico para que dure 1 segundo y lo guardo
fono_e_sint_1seg =abs( np.transpose(np.matlib.repmat(fono_e_sint,1,20) ))# le aplico el módulo porque son valores complejos con parte imaginario de aprox e-14

fig_e_sint, ax_e_sint = plt.subplots()
f_e_sint, t_e_sint, Sxx_e_sint = sg.spectrogram( fono_e_sint_1seg.reshape((16000,)), sampleRate, window = sg.get_window("hamming", 250))
ax_e_sint.pcolormesh(t_e_sint*1e3, f_e_sint, 10*np.log10(Sxx_e_sint),cmap = plt.get_cmap('jet'))
ax_e_sint.set_ylabel('Frecuencia [Hz]')
ax_e_sint.set_xlabel('Tiempo [ms]')
ax_e_sint.set_title("Espectrograma fono [e] sintetizado")

fig_e_sint.savefig("espect_fono_e_sint",dpi=300)

wfile.write("fono_e",sampleRate,fono_e_sint_1seg) 


########################################################################
########################################################################
## Multiplico el tren de pulsos glóticos con el filtro y le aplico ifft
########################################################################
fono_i_sint = np.fft.ifft(  np.multiply(tren_pulsos_fft,H_i_resp_freq[1])  )
#Lo hago periodico para que dure 1 segundo y lo guardo
fono_i_sint_1seg =abs( np.transpose(np.matlib.repmat(fono_i_sint,1,20) ))# le aplico el módulo porque son valores complejos con parte imaginario de aprox e-14

fig_i_sint, ax_i_sint = plt.subplots()
f_i_sint, t_i_sint, Sxx_i_sint = sg.spectrogram( fono_i_sint_1seg.reshape((16000,)), sampleRate, window = sg.get_window("hamming", 250))
ax_i_sint.pcolormesh(t_i_sint*1e3, f_i_sint, 10*np.log10(Sxx_i_sint),cmap = plt.get_cmap('jet'))
ax_i_sint.set_ylabel('Frecuencia [Hz]')
ax_i_sint.set_xlabel('Tiempo [ms]')
ax_i_sint.set_title("Espectrograma fono [i] sintetizado")

fig_i_sint.savefig("espect_fono_i_sint",dpi=300)


wfile.write("fono_i",sampleRate,fono_i_sint_1seg) 

########################################################################
########################################################################
## Multiplico el tren de pulsos glóticos con el filtro y le aplico ifft
########################################################################
fono_o_sint = np.fft.ifft(  np.multiply(tren_pulsos_fft,H_o_resp_freq[1])  )
#Lo hago periodico para que dure 1 segundo y lo guardo
fono_o_sint_1seg =abs( np.transpose(np.matlib.repmat(fono_o_sint,1,20) ))# le aplico el módulo porque son valores complejos con parte imaginario de aprox e-14

fig_o_sint, ax_o_sint = plt.subplots()
f_o_sint, t_o_sint, Sxx_o_sint = sg.spectrogram( fono_o_sint_1seg.reshape((16000,)), sampleRate, window = sg.get_window("hamming", 250))
ax_o_sint.pcolormesh(t_o_sint*1e3, f_o_sint, 10*np.log10(Sxx_o_sint),cmap = plt.get_cmap('jet'))
ax_o_sint.set_ylabel('Frecuencia [Hz]')
ax_o_sint.set_xlabel('Tiempo [ms]')
ax_o_sint.set_title("Espectrograma fono [o] sintetizado")

fig_o_sint.savefig("espect_fono_o_sint",dpi=300)

wfile.write("fono_o",sampleRate,fono_o_sint_1seg) 

########################################################################
########################################################################
## Multiplico el tren de pulsos glóticos con el filtro y le aplico ifft
########################################################################
fono_u_sint = np.fft.ifft(  np.multiply(tren_pulsos_fft,H_u_resp_freq[1])  )
#Lo hago periodico para que dure 1 segundo y lo guardo
fono_u_sint_1seg =abs( np.transpose(np.matlib.repmat(fono_u_sint,1,20) ))# le aplico el módulo porque son valores complejos con parte imaginario de aprox e-14

fig_u_sint, ax_u_sint = plt.subplots()
f_u_sint, t_u_sint, Sxx_u_sint = sg.spectrogram( fono_u_sint_1seg.reshape((16000,)), sampleRate, window = sg.get_window("hamming", 250))
ax_u_sint.pcolormesh(t_u_sint*1e3, f_u_sint, 10*np.log10(Sxx_u_sint),cmap = plt.get_cmap('jet'))
ax_u_sint.set_ylabel('Frecuencia [Hz]')
ax_u_sint.set_xlabel('Tiempo [ms]')
ax_u_sint.set_title("Espectrograma fono [o] sintetizado")

fig_u_sint.savefig("espect_fono_u_sint",dpi=300)


wfile.write("fono_u",sampleRate,fono_u_sint_1seg) 