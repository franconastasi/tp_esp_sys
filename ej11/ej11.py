#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 18 10:05:41 2018

@author: franco
"""

import scipy.io.wavfile as wfile

import matplotlib.pyplot as plt

import numpy as np

from scipy import signal as sg

from Psola import psola_freq




wavFile = "hh15.WAV"

sampleRate, data  = wfile.read(wavFile)
t = np.arange(len(data))/sampleRate


fig, ax = plt.subplots()
ax.plot(t,data, label="data")

ax.set(xlabel= "tiempo [s]", ylabel = "Amplitud", title = "Señal muestreada")


##################################################################
##################################################################

pts_sonoros=np.array( [ [ 0.5838 ,0.8385 ], [0.9438,1.0194], [1.2140 ,1.6425 ] , [1.6829 ,1.7403 ] , [1.9146 ,2.2643 ] , [ 2.4554, 2.5377] ,[2.6133 ,2.6945 ] ,[2.7907 ,2.9406 ] ,[ 2.9781,3.0986 ]   ] )



psola_freq_1_1 = psola_freq(data,pts_sonoros, porc = 1.1 )
fig1, ax1 = plt.subplots()
ax1.plot(t,psola_freq_1_1, label="data1")

ax1.set(xlabel= "tiempo [s]", ylabel = "Amplitud", title = "Señal muestreada con frecuencia fundamental reducida 90%")
fig1.savefig("freq_fund_90%",dpi=300)



psola_freq_1_2 = psola_freq(data,pts_sonoros, porc = 1.2 )
fig2, ax2 = plt.subplots()
ax2.plot(t,psola_freq_1_2, label="data2")

ax2.set(xlabel= "tiempo [s]", ylabel = "Amplitud", title = "Señal muestreada con frecuencia fundamental reducida 80%")
fig2.savefig("freq_fund_80%",dpi=300)


psola_freq_1_3 = psola_freq(data,pts_sonoros, porc = 1.3 )

fig3, ax3 = plt.subplots()
ax3.plot(t,psola_freq_1_3, label="data2")

ax3.set(xlabel= "tiempo [s]", ylabel = "Amplitud", title = "Señal muestreada con frecuencia fundamental reducida 70%")
fig3.savefig("freq_fund_70%",dpi=300)




fig4, ax4 = plt.subplots(2,2)
fig4.set_size_inches(9.5, 5.5)
fig4.suptitle("Comparación entre la señal luego del método PSOLA (ariba) y la señal original (abajo) \n para sonidos sonoros(Izquierda) y sonidos sordos(derechas)")
#fig4.subplots_adjust(top=2.8)


fig4.text(0.5,0.01,"Tiempo [s]")
fig4.text(0.01,0.5,"Amplitud", rotation="vertical")
ax4[0,0].plot(t[  (t>= 1.214)  &  (t<1.239) ],  psola_freq_1_1[ (t>= 1.214)  &  (t<1.239) ]  )
ax4[1,0].plot( t[ (t>= 1.214)  &  (t<1.239) ],  data[ (t>= 1.214)  &  (t<1.239) ]  )

ax4[0,1].plot(t[  (t>= 1.7403)  &  (t<1.7433) ],  psola_freq_1_1[ (t>= 1.7403)  &  (t<1.7433) ])
ax4[1,1].plot( t[ (t>= 1.7403)  &  (t<1.7433) ],  data[ (t>= 1.7403)  &  (t<1.7433) ]  )

#fig4.text(0.5,0.91,"Comparación entre la señal luego del método PSOLA (ariba) y la señal original (abajo) \n para sonidos sonoros(Izquierda) y sonidos sordos(derechas)")

fig4.tight_layout(pad=3.5)
fig4.savefig("comp_PSOLA",dpi=300)

########EJECUTAR HASTA ACA ####################
####### LO SIGUIENTE SON PRUEBAS #############










#Genero un array de ceros y lo leno con las partes no sonoras del sonido
Psola_array = np.zeros(len(data))

Psola_array[t<0.5838] = data[t<0.5838]

for idx, val in enumerate(pts_sonoros[0:len(pts_sonoros)-1]):
    Psola_array[(t>pts_sonoros[idx][1]) & (t<pts_sonoros[idx+1][0]) ] = data[(t>pts_sonoros[idx][1]) & (t<pts_sonoros[idx+1][0])]
    #print(idx, val[1])

Psola_array[t>3.0986] = data[t>3.0986]

###################

#Genero la ventana hamming con 80 puntos y un array auxiliar

hamm = sg.get_window("hamming",80)

aux_array =np.zeros(80)

t_max_aux = 0
max_aux =0

#Variable para indexar los periódos a multiplicar por hamming 
val_aux = 0

#Variable para indexar desde donde in sumando los valores que son la multiplicación de una ventana hamming por los períodos
new_val_aux = 0

t_fund = 0.005
n_fund = int(t_fund*16e3)

new_n_fund =int(n_fund*1.1)

new_t_fund =  new_n_fund/16e3
########################################3


for idx,val in enumerate(pts_sonoros):
    val_aux = val[0]
    new_val_aux = val[0]
    
    while(val_aux < val[1] + new_t_fund/2 ):
        if val_aux == val[0]:
            max_aux = np.argmax(data[  (t > val_aux) & (t < val_aux + t_fund)])
            max_aux += int(val_aux*16e3)  
            aux_array =np.multiply(data[max_aux - int(n_fund/2): max_aux + int(n_fund/2) ],hamm) 
            Psola_array[  (t >= val_aux) & (t < val_aux + t_fund) ] =  aux_array
            val_aux+=t_fund
            val_aux = np.round(val_aux,4)
            new_val_aux+= new_t_fund
            new_val_aux = np.round(new_val_aux,4)
        else:
            max_aux = np.argmax(data[  (t > val_aux) & (t < val_aux + t_fund)])
            max_aux += int(val_aux*16e3)
            aux_array =np.multiply(data[max_aux - int(n_fund/2): max_aux + int(n_fund/2) ],hamm)
            aux =  np.round(np.round(max_aux/16e3,4) - new_val_aux, 4) # Calculo la distancia al maximo para saber desde donde empezar a sumar
            Psola_array[  (t >= np.round(new_val_aux - aux,4) ) & (t <  np.round( new_val_aux - aux, 4 ) + (t_fund) ) ] +=  aux_array
            val_aux+=t_fund
            val_aux = np.round(val_aux,4)
            new_val_aux+= new_t_fund
            new_val_aux = np.round(new_val_aux,4)









plt.plot(Psola_array)
plt.plot(psola_freq_1_1)
plt.plot(psola_freq_1_2)
plt.plot(psola_freq_1_3)



