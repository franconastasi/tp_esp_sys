#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 17:16:14 2018

@author: franco
"""

import numpy as np
from scipy import signal as sg

def cepstrum(func):
    dft_funct = abs(np.fft.fft(func))
    
    return np.fft.ifft(np.log(dft_funct)).real