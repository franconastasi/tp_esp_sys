#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 20:11:25 2018

@author: franco
"""

import matplotlib.pyplot as plt

from scipy import signal as sg

import numpy as np

import scipy.io.wavfile as wfile

from cepstrum import cepstrum
#import cepstrum2  ## https://github.com/python-acoustics/python-acoustics/blob/master/acoustics/cepstrum.py

Fs = 16e3

wavFile = "hh15.WAV"

sampleRate, data  = wfile.read(wavFile)
t = np.arange(len(data))/sampleRate

fono_a = data[(t<1.02582) & (t>0.956)]
fono_e = data[(t<1.63646) & (t>1.58033)]
fono_i = data[(t<1.247) & (t>1.207)]
fono_o = data[(t<0.663486) & (t>0.588343)]


#Aplico la transformada cepstrum a cada fono, defino un array teniendo en cuenta que Fs = 16 kHz y grafico
cepstrum_fono_a= cepstrum(fono_a)
t_fono_a = np.arange(len(cepstrum_fono_a))/Fs

fig_a, ax_a = plt.subplots(2, 1)
ax_a[0].plot(t_fono_a*1e3,cepstrum_fono_a)
ax_a[0].set(xlabel="Tiempo {ms]", ylabel="Parte real de \n la transformada cepstrum",title = "Parte real de la transformada cepstrum - fono a")
ax_a[0].grid()

ax_a[1].plot(t_fono_a[(t_fono_a<0.02) & (t_fono_a>0.002) ]*1e3,cepstrum_fono_a[ (t_fono_a<0.02) & (t_fono_a>0.002)  ])
ax_a[1].set(xlabel="Tiempo {ms]", ylabel="Parte real de\n la transformada cepstrum")
ax_a[1].grid()

fig_a.tight_layout()


fig_a.savefig("cepstrum_hh15_a",dpi=300)



###########
##########


cepstrum_fono_e= cepstrum(fono_e)
t_fono_e = np.arange(len(cepstrum_fono_e))/Fs

fig_e, ax_e = plt.subplots(2, 1)
ax_e[0].plot(t_fono_e*1e3,cepstrum_fono_e)
ax_e[0].set(xlabel="Tiempo {ms]", ylabel="Parte real de \n la transformada cepstrum", title = "Parte real de la transformada cepstrum - fono e")
ax_e[0].grid()

ax_e[1].plot(t_fono_e[(t_fono_e<0.02) & (t_fono_e>0.002) ]*1e3,cepstrum_fono_e[ (t_fono_e<0.02) & (t_fono_e>0.002)  ])
ax_e[1].set(xlabel="Tiempo {ms]", ylabel="Parte real de\n la transformada cepstrum")
ax_e[1].grid()

fig_e.tight_layout()
fig_e.savefig("cepstrum_hh15_e",dpi=300)


###########
##########



cepstrum_fono_i= cepstrum(fono_i)
t_fono_i = np.arange(len(cepstrum_fono_i))/Fs

fig_i, ax_i = plt.subplots(2, 1)
ax_i[0].plot(t_fono_i*1e3,cepstrum_fono_i)
ax_i[0].set(xlabel="Tiempo {ms]", ylabel="Parte real de \n la transformada cepstrum", title = "Parte real de la transformada cepstrum - fono i")
ax_i[0].grid()

ax_i[1].plot(t_fono_i[(t_fono_i<0.02) & (t_fono_i>0.002) ]*1e3,cepstrum_fono_i[ (t_fono_i<0.02) & (t_fono_i>0.002)  ])
ax_i[1].set(xlabel="Tiempo {ms]", ylabel="Parte real de\n la transformada cepstrum")
ax_i[1].grid()

fig_i.tight_layout()
fig_i.savefig("cepstrum_hh15_i",dpi=300)






###########
##########


cepstrum_fono_o= cepstrum(fono_o)
t_fono_o = np.arange(len(cepstrum_fono_o))/Fs

fig_o, ax_o = plt.subplots(2, 1)
ax_o[0].plot(t_fono_o*1e3,cepstrum_fono_o)
ax_o[0].set(xlabel="Tiempo {ms]", ylabel="Parte real de \n la transformada cepstrum", title = "Parte real de la transformada cepstrum - fono o")
ax_o[0].grid()

ax_o[1].plot(t_fono_o[(t_fono_o<0.02) & (t_fono_o>0.002) ]*1e3,cepstrum_fono_o[ (t_fono_o<0.02) & (t_fono_o>0.002)  ])
ax_o[1].set(xlabel="Tiempo {ms]", ylabel="Parte real de\n la transformada cepstrum")
ax_o[1].grid()

fig_o.tight_layout()
fig_o.savefig("cepstrum_hh15_o",dpi=300)



########### HASTA ACA FUE SOLO CON LAS VOCALES, HAY QUE HACERLO CON TODO EL ARCHIVO



cant_muestras_25ms = int((25e-3)/(1/Fs))
hamm_wind = sg.get_window("hamming",cant_muestras_25ms)

quefren = np.arange(2e-3,8e-3, 1/Fs)

cesptrum_array = np.array([]).reshape(0,96)


for i in np.arange(0,50231):
    array_aux = data[i:i+cant_muestras_25ms+1]
    array_aux= np.array( [ cepstrum(array_aux)] )
    cesptrum_array= np.concatenate((cesptrum_array,np.array([array_aux[0,31:127] ])  ))


fig, [ax_señal, ax_cont] = plt.subplots(2,1)
ax_señal.plot(t,data)
ax_señal.set(xlabel="Tiempo [s]", ylabel = "Amplitud de la señal")
ax_cont.pcolormesh(t[0:50231],quefren*1e3,np.transpose(cesptrum_array),cmap=plt.get_cmap('jet'))
ax_cont.set(xlabel="Tiempo [s]", ylabel="Quefrencia [ms]")
ax_señal.grid()
ax_cont.grid()
fig.tight_layout()
fig.savefig("cont_freq_fund", dpi = 300)





# =============================================================================
# 
# fono_o = data[(t>0.611) & (t<0.657)]
# 
# cepstrum_fono_o= cepstrum(fono_o)
# t_fono_o = np.arange(len(cepstrum_fono_o))/Fs
# 
# fig_o, ax_o = plt.subplots(2, 1)
# ax_o[0].plot(t_fono_o*1e3,cepstrum_fono_o)
# ax_o[0].set(xlabel="Tiempo {ms]", ylabel="Parte real de \n la transformada cepstrum", title = "Parte real de la transformada cepstrum - fono o")
# ax_o[0].grid()
# 
# ax_o[1].plot(t_fono_o[(t_fono_o<0.02) & (t_fono_o>0.002) ]*1e3,cepstrum_fono_o[ (t_fono_o<0.02) & (t_fono_o>0.002)  ])
# ax_o[1].set(xlabel="Tiempo {ms]", ylabel="Parte real de\n la transformada cepstrum")
# ax_o[1].grid()
# 
# =============================================================================



#plt.plot(t_fono_a,cepstrum_fono_a)

#plt.plot(t_fono_a, cepstrum2.real_cepstrum(fono_a))




#lt.plot(2*np.pi/4 * np.arange(4),np.fft.fft(x))