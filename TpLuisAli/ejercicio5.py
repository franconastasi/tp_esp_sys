#!/usr/bin/python3
import soundfile as sf;
import matplotlib.pyplot as plt;
import numpy as npy;
from signalOperations import *;
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from scipy import signal;


if __name__ == '__main__':
    x, fs = sf.read('hh15.WAV');
    t= npy.linspace(0, 3,len(x));
    multipleSampleRangeTime={"O": [2.5014, 2.50668],"E": [1.22875, 1.23454],"A":[1.10049, 1.10575],"I":[1.74117, 1.74524]};
    fig = plt.figure()
    subplotIndex = 1;
    for letter, sampleRangeTime in multipleSampleRangeTime.items():
        xVocal, tx = signalExtractor(sampleRangeTime, t, x);
        spectrogramPlot(abs(xVocal),fs, subplotIndex, letter, fig, 2048);
        subplotIndex +=1;
    plt.show();
