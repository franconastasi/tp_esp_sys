#!/usr/bin/python3
import soundfile as sf;
import matplotlib.pyplot as plt;
import numpy as npy;
from arrows import *;
from SystemFilter import *;
from signalOperations import *;

if __name__ == '__main__':
    systemFilter = SystemFilter();
    glottalPulse = systemFilter.getGlottalPulse();
    fig = plt.figure()
    subplotIndex = 1;
    vowels = "aeio";
    for letter in vowels:
        totalHFreq = systemFilter.getTotalFilter(letter);
        allDenominatorsCoefficients = totalHFreq[2];
        glottalPulseTrain, t = glottalPulse.getTrainPulse();
        glottalPulseTrain = glottalPulseTrain - npy.mean(glottalPulseTrain);
        fs = glottalPulse.getSamplingFrequency();
        glottalPulseTrainCepstrum, glottalPulseTrainLog10AbsFourier = cepstrum(glottalPulseTrain);
        h = totalHFreq[0];
        Hlog10AbsFourier = npy.log10(abs(h));
        HCepstrum = npy.fft.ifft(Hlog10AbsFourier);
        y = signal.lfilter(npy.array([1, 0]), allDenominatorsCoefficients, glottalPulseTrain);
        YCepstrum, YLog10 = cepstrum(y);
        plt.plot(t, glottalPulseTrain);
        plt.show();
        plt.plot(glottalPulseTrainLog10AbsFourier);
        plt.show();
        plt.plot(t, glottalPulseTrainCepstrum);
        plt.show();
        #subplotIndex +=1;
