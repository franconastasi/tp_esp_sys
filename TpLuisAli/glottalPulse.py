#!/usr/bin/python3
import soundfile as sf;
import matplotlib.pyplot as plt;
import numpy as npy;
import math
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from scipy import signal;
class GlottalPulse():
    def __init__(self, pulseAmountInTrain=10):
        self.samplingFrequency = 16000;
        self.fundamentalFrequency = 200;
        self.glottalPulseDuration = 1/self.fundamentalFrequency;
        self.p0= 1;
        self.tp= self.glottalPulseDuration * 0.4;
        self.tn= self.glottalPulseDuration *0.16;
        finalTime = 2 # in seconds
        timestep = 1/self.samplingFrequency;
        self.t= [index*timestep for index in range(0, round(self.glottalPulseDuration*self.samplingFrequency))];
        self.glottalPulseTrainFrequency = round(self.samplingFrequency/self.fundamentalFrequency);
        self.p= [self.modelFunction(time) for time in self.t];
        self.p_DFT = npy.fft.fft(self.p);
        self.pulseAmountInTrain = pulseAmountInTrain;#10;
        deltaTrain= [1  if (((pulse) % (self.glottalPulseTrainFrequency)) == 0) else 0 for pulse in range(0, self.pulseAmountInTrain*self.glottalPulseTrainFrequency)];
        self.glottalPulseTrain = npy.convolve(self.p,deltaTrain);
        self.tConv= [index*timestep for index in range(0, len(self.p) + len(deltaTrain)-1)];
        self.glottalPulseTrainSpectrum = npy.fft.fft(self.glottalPulseTrain);
        self.frequencies = npy.fft.fftfreq(len(self.glottalPulseTrain), d= timestep);
        self.pFrequencies = npy.fft.fftfreq(len(self.p), d= timestep);

    def modelFunction(self, time):
      if (time >= 0 and time <= self.tp):
          return (self.p0/2)*(1- math.cos((time/self.tp)*math.pi));
      if (time > self.tp and time <= (self.tp + self.tn)):
          return self.p0*math.cos(((time-self.tp)/self.tn)*(math.pi/2));
      return 0;
    def getTrainPulseSpectrum(self):
        return self.glottalPulseTrainSpectrum, self.frequencies;
    def getTrainPulse(self):
        return self.glottalPulseTrain, self.tConv;
    def getPulseSpectrum(self):
        return self.p_DFT, self.pFrequencies;
    def getSamplingFrequency(self):
        return self.samplingFrequency;
    def getPulseAmountInTrain(self):
        return self.pulseAmountInTrain;
