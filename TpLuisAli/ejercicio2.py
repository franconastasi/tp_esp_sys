#!/usr/bin/python3
import soundfile as sf;
import matplotlib.pyplot as plt;
import numpy as npy;
from signalOperations import*

if __name__ == '__main__':
    x, fs = sf.read('hh15.WAV');
    t= npy.linspace(0, 3,len(x));
    multipleSampleRangeTime=[[0.840058, 0.844029],[1.10049, 1.10575],[1.8586, 1.86406]];
    for sampleRangeTime in multipleSampleRangeTime:
        fig = plt.figure()
        plt.subplot(2, 1, 1);
        plt.tight_layout(pad=2.6, w_pad=0.5, h_pad=2.0);
        xA, tA= signalExtractor(sampleRangeTime, t, x);
        coeficientesFourier = (npy.fft.fft(xA))/len(xA);
        plt.title('Vocal A');
        plt.xlabel('Tiempo[s]');
        plt.ylabel('X(t)');
        plt.plot(tA,xA);
        plt.subplot(2, 1, 2);
        plt.xlabel('k');
        plt.ylabel('|$a_k$|');
        plt.stem(abs(coeficientesFourier));
        plt.show();
