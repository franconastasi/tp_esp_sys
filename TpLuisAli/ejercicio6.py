#!/usr/bin/python3
from glottalPulse import *;


if __name__ == '__main__':
    glottalPulse = GlottalPulse();
    pulseAmountInTrain = glottalPulse.getPulseAmountInTrain();
    p_DFT, pFrequencies = glottalPulse.getPulseSpectrum();
    glottalPulseTrainSpectrum, frequencies = glottalPulse.getTrainPulseSpectrum();
    n= len(frequencies);
    n1 = len(pFrequencies);
    line1, =plt.plot(pFrequencies[:round(n1/2)],pulseAmountInTrain*abs(p_DFT[:round(n1/2)]),'r-');
    line2, =plt.plot(frequencies[:round(n/2)], abs(glottalPulseTrainSpectrum[:round(n/2)]),'-');
    plt.xlabel("Frecuenia [Hz]");
    plt.ylabel("|DFT|");
    plt.legend((line2, line1), ("|DFT de un tren de pulsos glóticos|", "|DFT de un pulso glótico|"))
    plt.show();
