import numpy as npy;
import matplotlib.pyplot as plt;
def signalExtractor(sampleRangeTime, t, x):
    tA= [tSample for tSample in t if ((tSample >= sampleRangeTime[0]) and (tSample <= sampleRangeTime[1]))];
    tStart, = npy.where(t == tA[0]);
    tStop = tStart[0] + len(tA);
    return x[tStart[0]: tStop], tA;

def spectrogramPlot(xVocal, fs, subplotIndex, letter, fig, windowSize=1024):
    plt.subplot(2, 2, subplotIndex);
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0);
    plt.title("Vocal "+ letter);

    #windowSize = 1024;
    nOverlap = round(windowSize/2);
    nFFT = windowSize;
    spectrum, freqs, t, cax = plt.specgram(xVocal, NFFT= windowSize, Fs = fs, window= npy.kaiser(windowSize, 6), noverlap= nOverlap);
    fig.colorbar(cax).set_label('Intensity [dB]');
    fig.tight_layout()
    plt.ylabel('Frequency [Hz]');
    plt.xlabel('Time [sec]');

def cepstrum(x):
    log10AbsFourier = npy.log10(abs(npy.fft.fft(x)));
    return npy.fft.ifft(log10AbsFourier), log10AbsFourier;
