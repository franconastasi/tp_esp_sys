#!/usr/bin/python3
import soundfile as sf;
import matplotlib.pyplot as plt;
import numpy as npy;
from arrows import *;
if __name__ == '__main__':
    x, fs = sf.read('hh15.WAV');
    t= npy.linspace(0, 3,len(x));
    signalFigure= plt.figure();
    signalFigure.suptitle('Señal de Audio', fontsize=14, fontweight='bold')
    ax = signalFigure.add_subplot(111);
    signalFigure.subplots_adjust(top=0.85);
    ax.set_title('Audio en texto');
    ax.set_xlabel('Tiempo[s]');
    ax.set_ylabel('X(t)');
    ax.set_xlim([0.3, 3]);
    ax.set_ylim([-0.45, 0.5]);
    tCoords = [0.49, 0.5, 0.65, 0.74, 0.81, 0.83, 0.89, 1.06, 1.07, 1.19, 1.30,1.36,1.45,1.47, 1.52, 1.68,1.82,1.92,2.01, 2.13, 2.15, 2.23, 2.26, 2.38, 2.44, 2.48, 2.58,2.62, 2.73];
    speech= "Con tagiaene rgiay fe r vo r"
    wordsLinePosition = -0.45;
    letterIndex=1;
    for xc in tCoords:
        ax.axvline(x=xc, c="red", linestyle="dashed");
    for letter in speech:
        if (letterIndex <= len(tCoords) -1):
            tPosition = tCoords[letterIndex -1] +(tCoords[letterIndex] - tCoords[letterIndex -1])/2;
            ax.text(tPosition,wordsLinePosition, letter, fontsize=15);
        letterIndex+=1;
    ax.plot(t,x);
    plt.show();
