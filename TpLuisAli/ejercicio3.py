#!/usr/bin/python3
import soundfile as sf;
import matplotlib.pyplot as plt;
import numpy as npy;
from signalOperations import*
def getSignalValue(n, filterfourierCoeficient, period):
    partialCumsum = npy.cumsum([coeficient*npy.exp((1j*k*2*npy.pi/period)*n) for k,coeficient in enumerate(filterfourierCoeficient)]).real;
    xn = partialCumsum[len(partialCumsum)-1];
    return xn;
def rebuildSignal(sampleRangeTime, t, x, fs, id):
    xA, tA = signalExtractor(sampleRangeTime, t, x);
    fourierCoeficients = (npy.fft.fft(xA))/len(xA);
    period = len(fourierCoeficients);
    filterfourierCoeficients = npy.asarray([abs(coeficient) if abs(coeficient) > 0.018 else 0 for coeficient in fourierCoeficients]);
    #plt.stem(filterfourierCoeficients);
    #letterDFT = fourierCoeficients * len(fourierCoeficients);
    #letterSignal = npy.fft.ifft(letterDFT);
    letterSignal = [getSignalValue(n, fourierCoeficients, period) for n in range(0, period)];
    resultSignal =[];
    for n in range(0,400):
        resultSignal.extend(letterSignal);
    sf.write('rebuildSignal_'+str(id)+'.WAV', resultSignal, fs);
    plt.plot(resultSignal);
    plt.show();

if __name__ == '__main__':
    x, fs = sf.read('hh15.WAV');
    t= npy.linspace(0, 3,len(x));
    multipleSampleRangeTime=[[0.840058, 0.844029],[1.10049, 1.10575],[1.8586, 1.86406]];
    id=0;
    for sampleRangeTime in multipleSampleRangeTime:
        rebuildSignal(sampleRangeTime, t, x, fs, id);
        id+=1;
