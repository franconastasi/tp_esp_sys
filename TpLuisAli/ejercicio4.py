#!/usr/bin/python3
import soundfile as sf;
import matplotlib.pyplot as plt;
import numpy as npy;
if __name__ == '__main__':
    x, fs = sf.read('HALUMNO.wav');
    t= npy.linspace(0, 3,len(x));
    signalFigure= plt.figure();
    signalFigure.suptitle('Señal de Audio', fontsize=14, fontweight='bold')
    ax = signalFigure.add_subplot(111);
    signalFigure.subplots_adjust(top=0.85);
    ax.set_title('Audio en texto');
    ax.set_xlabel('Tiempo[s]');
    ax.set_ylabel('X(t)');
    ax.set_xlim([0.3, 3]);
    ax.set_ylim([-0.45, 0.5]);
    wordsLinePosition = -0.45;
    letterIndex=1;
    ax.plot(t,x);
    plt.show();
