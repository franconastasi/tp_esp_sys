#!/usr/bin/python3
import numpy as npy;
from glottalPulse import *;
class SystemFilter():
    def __init__(self, pulseAmountInTrain=10):
        self.HTotal = npy.array([]);
        self.linesPlot=[];
        self.f = npy.array([]);
        self.allDenominatorsCoefficients = [];
        self.denominatorPolynomialCoefficients=[];
        self.partialFilter = {"a": [], "e": [], "i": [], "o": [],"u":[]};
        self.totalFilter = {"a": [], "e": [], "i": [], "o": [],"u":[]};
        self.glottalPulse = GlottalPulse(pulseAmountInTrain);
        self.Fs = self.glottalPulse.getSamplingFrequency();
        self.numerator= npy.array([1, 0]);
        vowelParameters = {"a": [[830,110], [1400, 160],[2890, 210],[3930, 230]],
                            "e": [[500,80], [2000, 156], [3130, 190],[4150, 220]],
                            "i": [[500, 80],[2000, 156], [3130, 190], [4150, 220]],
                            "o": [[546,97],[934,130], [2966, 185], [3930, 240]],
                            "u": [[382, 74], [740, 150], [2760, 210], [3380, 180]]};
        for vowel, currentvowelParameters in vowelParameters.items():
            self.allDenominatorsCoefficients = [];
            for frequencyBandwidthPair in currentvowelParameters:
                Fn=frequencyBandwidthPair[0];
                Bn=frequencyBandwidthPair[1];
                formantPole = math.exp(-2*math.pi*Bn/self.Fs)*(math.cos(2*math.pi*Fn/self.Fs) + 1j* math.sin(2*math.pi*Fn/self.Fs));
                b1 = [1, -formantPole];
                b2 = [1, -formantPole.conjugate()];
                self.denominatorPolynomialCoefficients = npy.convolve(b1, b2);
                f, h = self.getFilterSpectrumAndFrequencies(self.denominatorPolynomialCoefficients);
                self.partialFilter[vowel].append([f, h]);
                self.computeAllDenominatorCoefficiente();
            f, h = self.getFilterSpectrumAndFrequencies(self.allDenominatorsCoefficients);
            self.totalFilter[vowel]= [f, h, self.allDenominatorsCoefficients];
    def getFilterSpectrumAndFrequencies(self, denominatorCoefficents):
        w, h = signal.freqz(self.numerator, denominatorCoefficents);
        f = self.Fs * w / (2*math.pi);
        return f, h;
    def computeAllDenominatorCoefficiente(self):
        if (len(self.allDenominatorsCoefficients) == 0):
            self.allDenominatorsCoefficients.extend(self.denominatorPolynomialCoefficients.tolist());
        else:
            self.allDenominatorsCoefficients = npy.convolve(self.allDenominatorsCoefficients, self.denominatorPolynomialCoefficients);
    def getTotalFilter(self, vowel):
        return self.totalFilter[vowel];
    def getPartialFilter(self, vowel):
        return self.partialFilter[vowel];
    def getAllDenominators(self):
        return self.allDenominatorsCoefficients;
    def getGlottalPulse(self):
        return self.glottalPulse;
