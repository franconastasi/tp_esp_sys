#!/usr/bin/python3
import matplotlib.pyplot as plt;
import matplotlib.gridspec as gs;
import soundfile as sf;
from SystemFilter import *;
from signalOperations import *;

if __name__ == '__main__':
    systemFilter = SystemFilter(200);
    glottalPulse = systemFilter.getGlottalPulse();
    fig = plt.figure()
    subplotIndex = 1;
    vowels = "aeio";
    for letter in vowels:
        totalHFreq = systemFilter.getTotalFilter(letter);
        allDenominatorsCoefficients = totalHFreq[2];
        glottalPulseTrain, t = glottalPulse.getTrainPulse();
        glottalPulseTrain = glottalPulseTrain - npy.mean(glottalPulseTrain);
        fs = glottalPulse.getSamplingFrequency();
        y = signal.lfilter(npy.array([1, 0]), allDenominatorsCoefficients, glottalPulseTrain);
        y= y / abs(y).max();#npy.linalg.norm(y.real);
        spectrogramPlot(y.real,fs, subplotIndex, letter, fig);
        sf.write('rebuildSignalSynthetizied_'+str(subplotIndex)+'.WAV', y.real, fs);
        subplotIndex +=1;
    plt.show();
