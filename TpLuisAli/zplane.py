#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as pl
from  matplotlib import patches
from matplotlib.figure import Figure
from matplotlib import rcParams
from arrows import *;
def zplane(b,a,fig, ax):
    """Plot the complex z-plane given a transfer function.
    """

    # get a figure/plot
    #fig = pl.figure();
    #ax = fig.add_subplot(111)

    # create the unit circle
    uc = patches.Circle((0,0), radius=1, fill=False,
                        color='black', ls='dashed')
    ax.add_patch(uc)

    # The coefficients are less than 1, normalize the coeficients
    if np.max(b) > 1:
        kn = np.max(b)
        b = b/float(kn)
    else:
        kn = 1

    if np.max(a) > 1:
        kd = np.max(a)
        a = a/float(kd)
    else:
        kd = 1

    # Get the poles and zeros
    p = np.roots(a)
    z = np.roots(b)
    k = kn/float(kd)

    # Plot the zeros and set marker properties
    t1 = pl.plot(z.real, z.imag, 'go', ms=10)
    pl.setp( t1, markersize=10.0, markeredgewidth=1.0,
              markeredgecolor='k', markerfacecolor='g')

    # Plot the poles and set marker properties
    t2 = pl.plot(p.real, p.imag, 'rx', ms=10)
    pl.setp( t2, markersize=12.0, markeredgewidth=3.0,
              markeredgecolor='r', markerfacecolor='r')

    ax.spines['left'].set_position('center')
    pl.ylabel('$Im\{z\}$')
    ax.spines['bottom'].set_position('center')
    pl.xlabel('$Re\{z\}$')
    ax.yaxis.set_label_coords(0.48,1);
    ax.xaxis.set_label_coords(1,0.48);
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    # set the ticks
    r = 1.5;
    pl.axis('scaled');
    pl.axis([-r, r, -r, r])
    ticks = [-1, -.5, .5, 1];
    pl.xticks(ticks);
    pl.yticks(ticks)
    setArrows(ax, fig);
    pl.title("Diagrama de polos y ceros de H(z)");
    """xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    # get width and height of axes object to compute
    # matching arrowhead length and width
    dps = fig.dpi_scale_trans.inverted()
    bbox = ax.get_window_extent().transformed(dps)
    width, height = bbox.width, bbox.height

    # manual arrowhead width and length
    hw = 1./30.*(ymax-ymin)
    hl = 1./30.*(xmax-xmin)
    lw = 1. # axis line width
    ohg = 0.3 # arrow overhang

    # compute matching arrowhead length and width
    yhw = hw/(ymax-ymin)*(xmax-xmin)* height/width
    yhl = hl/(xmax-xmin)*(ymax-ymin)* width/height

    # draw x and y axis
    ax.arrow(xmin, 0, xmax-xmin, 0., fc='k', ec='k', lw = lw,
             head_width=hw, head_length=hl, overhang = ohg,
             length_includes_head= True, clip_on = False)

    ax.arrow(0, ymin, 0., ymax-ymin, fc='k', ec='k', lw = lw,
             head_width=yhw, head_length=yhl, overhang = ohg,
             length_includes_head= True, clip_on = False)
    if filename is None:
        pl.show()
    else:
        pl.savefig(filename)"""


    return z, p, k
