#!/usr/bin/python3
from glottalPulse import *;
from scipy import *;
from zplane import *;
from peakDetection import *;
from SystemFilter import *;
import matplotlib.pylab as pl
import matplotlib.gridspec as gridspec


def writePeaksOnPlot(HTotal, f):
    maxPeaks, minPeaks = peakdet(HTotal,.001);
    maxFrequencies = [ f[int(pos)] for pos in array(maxPeaks)[:,0]]
    pl.scatter(maxFrequencies, array(maxPeaks)[:,1], color='blue');
    frequencyIndex = 1;
    for currentFrequency, h in zip(maxFrequencies, array(maxPeaks)[:,1]):
        pl.annotate('F$_'+str(frequencyIndex)+'$= '+' '+str(currentFrequency)+' Hz',
         xy=(currentFrequency, h), xycoords='data',
         xytext=(+15, +20), textcoords='offset points', fontsize=7,
         arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.2"));
        frequencyIndex += 1;
def plotAnswerToPulseTrain(systemFilter):
    glottalPulse = systemFilter.getGlottalPulse();
    totalHFreq = systemFilter.getTotalFilter(letter);
    allDenominatorsCoefficients = totalHFreq[2];
    ax = pl.subplot(gs[1, 0]) # row 0, col 1
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    glottalPulseTrain, t = glottalPulse.getTrainPulse();
    y = signal.lfilter(npy.array([1, 0]), allDenominatorsCoefficients, glottalPulseTrain);
    pl.plot(t, y.real);
    pl.title("Salida al tren de pulsos glótico");
    pl.xlabel("Tiempo [s]");
    pl.ylabel("y(t)");
    ax = pl.subplot(gs[1, 1]) # row 0, col 1
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    y_DFT = npy.fft.fft(y);
    timestep = abs(t[1] - t[0]);
    freq = np.fft.fftfreq(len(y), d=timestep);
    pl.plot(freq[:round(len(freq)/2)], abs(y_DFT)[:round(len(y_DFT)/2)]);
    pl.title("Espectro de amplitud de la salida al tren de pulsos glótico");
    pl.xlabel("Frecuencia [Hz]");
    pl.ylabel("|y(w)|");

def plotTotalFilter(HTotal, vowels, vowelsIndex, linesPlot):
    lineTotal, = pl.plot(f, npy.abs(HTotal), linewidth=2);
    linesPlot.append(lineTotal);
    pl.legend(linesPlot, ["$|H_1(w)|$", "$|H_2(w)|$", "$|H_3(w)|$", "$|H_4(w)|$", "$|H(w)|$"]);
    pl.title("Respuesta en frecuencia del filtro resultante H(w) y los filtros parciales $H_n(w)$");
    pl.xlabel('Frecuencia [Hz]')
    pl.ylabel('$|H(w)|$');

if __name__ == '__main__':
    systemFilter = SystemFilter();
    vowels = "aeiou";
    for letter in vowels:
        gs = gridspec.GridSpec(3, 2)
        gs.update(left=0.1,right=0.9,top=0.9,bottom=0.03,wspace=0.3,hspace=0.5)
        fig = pl.figure()
        pl.suptitle("Modelización de una "+ letter);
        ax = pl.subplot(gs[0, :]) # row 0, span all columns
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False);
        totalHFreq = systemFilter.getTotalFilter(letter);
        partialFilter = systemFilter.getPartialFilter(letter);
        linesPlot = [];
        for partialFilterFreqResponse in partialFilter:
            f= partialFilterFreqResponse[0];
            h = partialFilterFreqResponse[1];
            line, = pl.plot(f, npy.abs(h),linestyle='dashed', linewidth=0.5);
            linesPlot.append(line);
        writePeaksOnPlot(npy.abs(totalHFreq[1]), totalHFreq[0]);
        lineTotal, = pl.plot(totalHFreq[0], npy.abs(totalHFreq[1]), linewidth=2);
        linesPlot.append(lineTotal);
        pl.legend(linesPlot, ["$|H_1(w)|$", "$|H_2(w)|$", "$|H_3(w)|$", "$|H_4(w)|$", "$|H(w)|$"]);
        pl.title("Respuesta en frecuencia del filtro resultante H(w) y los filtros parciales $H_n(w)$");
        pl.xlabel('Frecuencia [Hz]')
        pl.ylabel('$|H(w)|$');
        plotAnswerToPulseTrain(systemFilter);
        ax = pl.subplot(gs[2, :])
        zplane(npy.array([1, 0]), npy.array([coeficient.real for coeficient in totalHFreq[2]]), fig, ax);
        pl.show();
