#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 10 16:59:41 2018

@author: franco
"""
import scipy.io.wavfile as wfile

import matplotlib.pyplot as plt

import numpy as np

from scipy import signal as sg

wavFile = "hh15.WAV"

sampleRate, data  = wfile.read(wavFile)
t = np.arange(len(data))/sampleRate

#Defino los tiempos iniciales y finales de 3 vocales a partir de lo
#observado en el gráfico del ej1

# o in  0.581159 <= t < 0.666431

# a in 0.944199 <= t < 1.02998

# e in 1.55511 <= t < 1.64288


t_o_ini = 0.581159
t_o_fin = 0.666431

t_a_ini = 0.944199
t_a_fin = 1.02998

t_e_ini = 1.55511
t_e_fin = 1.64288


fig, (ax_o, ax_a, ax_e) = plt.subplots(1, 3, figsize=(12, 9))

##Gráfico cada uno de los espectrogtramas

f_o, t_o, Sxx_o = sg.spectrogram(data[ (t >= t_o_ini) & (t < t_o_fin) ], sampleRate, window = sg.get_window("hamming", 250))
ax_o.pcolormesh(t_o*1e3, f_o, 10*np.log10(Sxx_o),cmap = plt.get_cmap('jet'))
ax_o.set_ylabel('Frecuencia [Hz]')
ax_o.set_xlabel('Tiempo [ms]')
ax_o.set_title("Espectrograma \n fono [o]")


f_a, t_a, Sxx_a = sg.spectrogram(data[ (t >= t_a_ini) & (t < t_a_fin) ], sampleRate, window = sg.get_window("hamming", 250))

ax_a.pcolormesh(t_a*1e3, f_a, 10*np.log10(Sxx_a),cmap = plt.get_cmap('jet'))
ax_a.set_ylabel('Frecuencia [Hz]')
ax_a.set_xlabel('Tiempo [ms]')
ax_a.set_title("Espectrograma \n fono [a]")


f_e, t_e, Sxx_e = sg.spectrogram(data[ (t >= t_e_ini) & (t < t_e_fin) ], sampleRate, window = sg.get_window("hamming", 250))

ax_e.pcolormesh(t_e*1e3, f_e, 10*np.log10(Sxx_e),cmap = plt.get_cmap('jet'))
ax_e.set_ylabel('Frecuencia [Hz]')
ax_e.set_xlabel('Tiempo [ms]')
ax_e.set_title("Espectrograma \n fono [e]")

fig.tight_layout()

fig.savefig("espect",dpi = 300)


##Me fijo el ancho de anda de la ventana hamming

vent_long = 250
hamm = sg.get_window("hamming",vent_long)
t_vent = np.arange(0,vent_long)/sampleRate


plt.plot(t_vent*1e3,hamm)

espec_vent = np.fft.fft(hamm)

t_espec_vent = np.arange(0,2*np.pi,2*np.pi/vent_long)
plt.stem(t_espec_vent*(sampleRate/(2*np.pi)),abs(espec_vent))

bd = t_espec_vent[1]*sampleRate/(2*np.pi)
