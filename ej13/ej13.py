#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 19 16:40:45 2018

@author: franco
"""


############################## NO EJECUTAR EL SCRIPT CON F5  ###################################
############################## NO EJECUTAR EL SCRIPT CON F5  ###################################
############################## NO EJECUTAR EL SCRIPT CON F5  ###################################

import os

import matplotlib.pyplot as plt

from scipy import signal as sg

import numpy as np

import scipy.io.wavfile as wfile


########################################################################
#################### EJECUTAR EJ6, Ej7BIS y EJ8     ####################
#################### ANTES DE EJECUTAR ESTE ARCHIVO ####################
########################################################################

### Esta función tien como argumentos el array con el fono sintetizado (ej: fono_a_sint), y el nombre del archivo de audio a grabar

def aum_ffund(fono_array, fono_string):
    array_aux = fono_array[360:440]
    max_idx = np.argmax(array_aux)
    
    fono_1_per_aux =  np.multiply( abs(fono_array[ (360+max_idx -40): 360+max_idx +40 ]), sg.get_window("hamming",80))
       
    
    ## CADA 40 ms se reduce el período de la señal
    fono_mod_F0 = np.zeros( int(4.04*Fs))
    idx = 0
    idx_aux_retroceso = 0
    for i in np.arange(0,101):
        idx_aux_retroceso =  int(round(Fs/F0 - Fs/(F0+i)))
        while(idx < (i+1)*(40/1e3)*Fs):
            if(idx == 64600):
                fono_mod_F0[idx - idx_aux_retroceso : idx - idx_aux_retroceso + 80] += fono_1_per_aux[0:67]
                idx= idx - idx_aux_retroceso + 80
            else:
                fono_mod_F0[idx - idx_aux_retroceso : idx - idx_aux_retroceso + 80] += fono_1_per_aux
                idx= idx - idx_aux_retroceso + 80
    
    t_mod = np.arange(len(fono_mod_F0))/(16e3)
    fig,ax = plt.subplots()
    fig.set_size_inches(10,6)
    ax.set(xlabel="Tiempo [ms]",ylabel= "Amplitud",
           Title= "Comparación del fono [" + fono_string[5] + "] con su frecuencia fundametal aumentada a 300 Hz" )
    ax.plot(1e3*t_mod[0:240],fono_array[0:240],'k',
            label= "Fono original")
    ax.plot(1e3*t_mod[0:240],fono_mod_F0[64000:64240],'',
            label= "Fono original con frecuencia fundamental aumentada")
    
    #ax.set_ylim(top=25)
    ax.legend()
    fig.savefig(fono_string + "aum", dpi = 300)
            
    wfile.write(fono_string + "_300Hz",int(Fs),fono_mod_F0)  

def red_ffund(fono_array, fono_string):
    array_aux = fono_array[360:440]
    max_idx = np.argmax(array_aux)
    
    fono_1_per_aux =  np.multiply( abs(fono_array[ (360+max_idx -40): 360+max_idx +40 ]), sg.get_window("hamming",80))
         
    ## CADA 40 ms se reduce el período de la señal
    fono_mod_F0 = np.zeros( int(4.04*Fs))
    idx = 0
    idx_aux_adelanto = 0
    for i in np.arange(0,101):
        idx_aux_adelanto =  int(round(Fs/(F0-i) - Fs/F0 ))
        while(idx < (i+1)*(40/1e3)*Fs):
            if(idx == 64628):
                fono_mod_F0[idx : idx + 80] += fono_1_per_aux
                idx= idx + idx_aux_adelanto + 80
            else:
                fono_mod_F0[idx : idx + 80] += fono_1_per_aux
                idx= idx + idx_aux_adelanto + 80
    
    t_mod = np.arange(len(fono_mod_F0))/(16e3)
    fig,ax = plt.subplots()
    fig.set_size_inches(10,6)
    ax.set(xlabel="Tiempo [ms]",ylabel= "Amplitud",
           Title= "Comparación del fono [" + fono_string[5] + "] con su frecuencia fundametal reducida a 100 Hz" )
    ax.plot(1e3*t_mod[0:480],fono_array[0:480],'k',
            label= "Fono original")
    ax.plot(1e3*t_mod[0:480],fono_mod_F0[64000:64480],'',
            label= "Fono original con frecuencia fundamental reducida")
    
    #ax.set_ylim(top=25)
    ax.legend()
    fig.savefig(fono_string + "red", dpi = 300)
        
    wfile.write(fono_string+"_200Hz",int(Fs),fono_mod_F0)     
        


## Aplico las funciones para cada una de las vocales
    
    
##################### FONO A #####################    
    
aum_ffund(fono_a_sint,"fono_a_sint")
red_ffund(fono_a_sint,"fono_a_sint")


##################### FONO E #####################     
aum_ffund(fono_e_sint,"fono_e_sint")
red_ffund(fono_e_sint,"fono_e_sint")


##################### FONO I #####################     
aum_ffund(fono_i_sint,"fono_i_sint")
red_ffund(fono_i_sint,"fono_i_sint")


##################### FONO O #####################     
aum_ffund(fono_o_sint,"fono_o_sint")
red_ffund(fono_o_sint,"fono_o_sint")


##################### FONO U #####################     
aum_ffund(fono_u_sint,"fono_u_sint")
red_ffund(fono_u_sint,"fono_u_sint")






















############################## CODIGO APARTE PARA PROBAR LAS FUNCIONES #########################
############################## NO EJECUTAR EL SCRIPT CON F5  ###################################




t_1_seg = np.arange(0,1,1/Fs)

Fs = 16e3
F0 = 200

################ FONO A ####################

#Obtengo el máximo lo multiplico por una ventana y me quedo con ese array #######



array_aux = fono_a_sint[360:440]
max_idx = np.argmax(array_aux)

fono_a_1_per_aux =  np.multiply( abs(fono_a_sint[ (360+max_idx -40): 360+max_idx +40 ]), sg.get_window("hamming",80))

fono_a_mod_F0 = np.zeros( int(4.04*Fs))


## CADA 40 ms se reduce el período de la señal
fono_a_mod_F0 = np.zeros( int(4.04*Fs))
idx = 0
idx_aux_retroceso = 0
for i in np.arange(0,101):
    idx_aux_retroceso =  int(round(Fs/F0 - Fs/(F0+i)))
    while(idx < (i+1)*(40/1e3)*Fs):
        if(idx == 64600):
            fono_a_mod_F0[idx - idx_aux_retroceso : idx - idx_aux_retroceso + 80] += fono_a_1_per_aux[0:67]
            idx= idx - idx_aux_retroceso + 80
        else:
            fono_a_mod_F0[idx - idx_aux_retroceso : idx - idx_aux_retroceso + 80] += fono_a_1_per_aux
            idx= idx - idx_aux_retroceso + 80
        
wfile.write("fono_a_F0_300Hz",int(Fs),fono_a_mod_F0)     
        





## CADA 40 ms se aumenta el período de la señal
fono_a_mod_F0 = np.zeros( int(4.04*Fs))
idx = 0
idx_aux_adelanto = 0
for i in np.arange(0,101):
    idx_aux_adelanto =  int(round(Fs/(F0-i) - Fs/F0 ))
    while(idx < (i+1)*(40/1e3)*Fs):
        if(idx == 64628):
            fono_a_mod_F0[idx : idx + 80] += fono_a_1_per_aux
            idx= idx + idx_aux_adelanto + 80
        else:
            fono_a_mod_F0[idx : idx + 80] += fono_a_1_per_aux
            idx= idx + idx_aux_adelanto + 80
        
wfile.write("fono_a_F0_200Hz",int(Fs),fono_a_mod_F0)     
        










################ FONO E ####################

#Obtengo el máximo lo multiplico por una ventana y me quedo con ese array #######



array_aux = fono_e_sint[360:440]
max_idx = np.argmax(array_aux)

fono_e_1_per_aux =  np.multiply( abs(fono_e_sint[ (360+max_idx -40): 360+max_idx +40 ]), sg.get_window("hamming",80))


## CADA 40 ms se reduce el período de la señal
fono_e_mod_F0 = np.zeros( int(4.04*Fs))
idx = 0
idx_aux_retroceso = 0
for i in np.arange(0,101):
    idx_aux_retroceso =  int(round(Fs/F0 - Fs/(F0+i)))
    while(idx < (i+1)*(40/1e3)*Fs):
        if(idx == 64600):
            fono_e_mod_F0[idx - idx_aux_retroceso : idx - idx_aux_retroceso + 80] += fono_e_1_per_aux[0:67]
            idx= idx - idx_aux_retroceso + 80
        else:
            fono_e_mod_F0[idx - idx_aux_retroceso : idx - idx_aux_retroceso + 80] += fono_e_1_per_aux
            idx= idx - idx_aux_retroceso + 80
        
wfile.write("fono_e_F0_300Hz",int(Fs),fono_e_mod_F0)     
        


## CADA 40 ms se aumenta el período de la señal
fono_a_mod_F0 = np.zeros( int(4.04*Fs))
idx = 0
idx_aux_adelanto = 0
for i in np.arange(0,101):
    idx_aux_adelanto =  int(round(Fs/(F0-i) - Fs/F0 ))
    while(idx < (i+1)*(40/1e3)*Fs):
        if(idx == 64628):
            fono_a_mod_F0[idx : idx + 80] += fono_a_1_per_aux
            idx= idx + idx_aux_adelanto + 80
        else:
            fono_a_mod_F0[idx : idx + 80] += fono_a_1_per_aux
            idx= idx + idx_aux_adelanto + 80
        
wfile.write("fono_a_F0_200Hz",int(Fs),fono_a_mod_F0)     
        





def aum_ffund(fono_array, fono_string):
    array_aux = fono_array[360:440]
    max_idx = np.argmax(array_aux)
    
    fono_1_per_aux =  np.multiply( abs(fono_array[ (360+max_idx -40): 360+max_idx +40 ]), sg.get_window("hamming",80))
       
    
    ## CADA 40 ms se reduce el período de la señal
    fono_mod_F0 = np.zeros( int(4.04*Fs))
    idx = 0
    idx_aux_retroceso = 0
    for i in np.arange(0,101):
        idx_aux_retroceso =  int(round(Fs/F0 - Fs/(F0+i)))
        while(idx < (i+1)*(40/1e3)*Fs):
            if(idx == 64600):
                fono_mod_F0[idx - idx_aux_retroceso : idx - idx_aux_retroceso + 80] += fono_1_per_aux[0:67]
                idx= idx - idx_aux_retroceso + 80
            else:
                fono_mod_F0[idx - idx_aux_retroceso : idx - idx_aux_retroceso + 80] += fono_1_per_aux
                idx= idx - idx_aux_retroceso + 80
            
    wfile.write("fono_string" + "_300Hz",int(Fs),fono_a_mod_F0)  










plt.plot(fono_a_mod_F0[64000:64640])
plt.plot(fono_a_mod_F0[0:640])
plt.plot(fono_a_mod_F0)



x= np.zeros(240)
x[0:80]+= fono_a_1_per_aux
x[80- idx_aux_retroceso :160-idx_aux_retroceso]+= fono_a_1_per_aux
x[160- 2*idx_aux_retroceso :240-2*idx_aux_retroceso]+= fono_a_1_per_aux
plt.plot(x)

#fono_a_sint_1_per = fono_a_sint[0:80]
