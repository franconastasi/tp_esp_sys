#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  7 23:56:46 2018

@author: franco
"""

import scipy.io.wavfile as wfile

import matplotlib.pyplot as plt

import numpy as np

wavFile = "hh15.WAV"

sampleRate, data  = wfile.read(wavFile)
t = np.arange(len(data))/sampleRate


#Defino los tiempos iniciales y finales para 1 períodos y múltiples (5) períodos 
#basado en el gráfico del ejercicio 1

t_init_a_1_per = 0.966672
t_fin_a_1_per  = 0.97151



t_init_a_mul_per = 0.966672
t_fin_a_mul_per = 0.985419


#obtengo los arreglos correspondientes a 1 período y a múltiples del fono []

a_1_per = data[ (t> t_init_a_1_per) & (t< t_fin_a_1_per)]
a_1_per_lenght = len(a_1_per)
t_a_1_per = np.arange(0,2*np.pi, 2*np.pi/a_1_per_lenght)

a_mul_per = data[ (t> t_init_a_mul_per) & (t< t_fin_a_mul_per)]
a_mul_per_lenght = len(a_mul_per)
t_a_mul_per = np.arange(0,2*np.pi, 2*np.pi/a_mul_per_lenght)


#Calculo los coeficientes de fourier y los gráfico

a_1_per_fft = np.fft.fft(a_1_per)/a_1_per_lenght

fig1, ax1 =plt.subplots()
ax1.set(xlabel = "Frecuencia", ylabel = "Coeficientes de fourier", title = "Coeficientes de Fourier del fono [a] - 1 período muestreado")
ax1.stem(t_a_1_per, abs(a_1_per_fft))
fig1.savefig("coef_four_1_per", dpi =  300)


a_mul_per_fft = np.fft.fft(a_mul_per)/a_mul_per_lenght

fig2, ax2 =plt.subplots()
ax2.set(xlabel = "Frecuencia", ylabel = "Coeficientes de fourier", title = "Coeficientes de Fourier del fono [a] - 5 períodos muestreado")
ax2.stem(t_a_mul_per,abs(a_mul_per_fft))
fig2.savefig("coef_four_mul_per", dpi =  300)






