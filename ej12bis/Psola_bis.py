#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 17 13:10:41 2018

@author: franco
"""

import scipy.io.wavfile as wfile

import matplotlib.pyplot as plt

import numpy as np

from scipy import signal as sg


def psola_dur(data,pts_sonoros, porc = 1.1 ):
    
    wavFile = "hh15.WAV"

    sampleRate, data  = wfile.read(wavFile)
    t = np.arange(len(data))/sampleRate
        
    Psola_array = np.zeros(len(data))
    Psola_array[t<0.5838] = data[t<0.5838]

    for idx, val in enumerate(pts_sonoros[0:len(pts_sonoros)-1]):
        Psola_array[(t>pts_sonoros[idx][1]) & (t<pts_sonoros[idx+1][0]) ] = data[(t>pts_sonoros[idx][1]) & (t<pts_sonoros[idx+1][0])]
        #print(idx, val[1])
    
    Psola_array[t>3.0986] = data[t>3.0986]
    
    
    hamm = sg.get_window("hamming",80)

    aux_array =np.zeros(80)
    
    #t_max_aux = 0
    max_aux =0
    
    #Variable para indexar los periódos a multiplicar por hamming 
    val_aux = 0
    
    #Variable para indexar desde donde in sumando los valores que son la multiplicación de una ventana hamming por los períodos
    #new_val_aux = 0
    
    t_fund = 0.005
    n_fund = int(t_fund*16e3)
    
    ########################################3
    
    
    for idx,val in enumerate(pts_sonoros):
        val_aux = val[0]        
        while(val_aux < val[1] + t_fund/2 ):
            max_aux = np.argmax(data[  (t > val_aux) & (t < val_aux + t_fund)])
            max_aux += int(val_aux*16e3)  
            aux_array =np.multiply(data[max_aux - int(n_fund/2): max_aux + int(n_fund/2) ],hamm) 
            #aux =  np.round(np.round(max_aux/16e3,4) - new_val_aux, 4) # Calculo la distancia al maximo para saber desde donde empezar a sumar
            #Psola_array[  (t >= val_aux) & (t < val_aux + t_fund) ] =  aux_array
            #Psola_array[  (t >= np.round(val_aux - aux,4) ) & (t <  np.round( new_val_aux - aux, 4 ) + (t_fund) ) ] +=  aux_array
            Psola_array[ max_aux - int(n_fund/2) : max_aux + int(n_fund/2)  ] +=  aux_array
            val_aux+=t_fund
            val_aux = np.round(val_aux,4)
    for idx,val in enumerate(pts_sonoros):
        val_final = val[1]
        prim_idx = np.arange( int(val[0]*16e3), int(val[0]*16e3) +n_fund)
        while(val_final - val[0]) > porc*(val[1]- val[0]):
            Psola_array = np.delete(Psola_array,prim_idx)
            val_final -= t_fund
    
    return Psola_array