#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 18 18:04:37 2018

@author: franco
"""


import scipy.io.wavfile as wfile

import matplotlib.pyplot as plt

import numpy as np

from scipy import signal as sg

from Psola_bis import psola_dur


wavFile = "hh15.WAV"

sampleRate, data  = wfile.read(wavFile)
t = np.arange(len(data))/sampleRate

##################################################################
##################################################################

pts_sonoros=np.array( [ [ 0.5838 ,0.8385 ], [0.9438,1.0194], [1.2140 ,1.6425 ] , [1.6829 ,1.7403 ] , [1.9146 ,2.2643 ] , [ 2.4554, 2.5377] ,[2.6133 ,2.6945 ] ,[2.7907 ,2.9406 ] ,[ 2.9781,3.0986 ]   ] )



 #==================
psola_dur_0_9 = psola_dur(data,pts_sonoros,porc = 0.9)
new_t_1_1 = np.arange(len(psola_dur_0_9))/16e3

fig1, ax1 = plt.subplots()
ax1.plot(new_t_1_1,psola_dur_0_9, label="data1")

ax1.set(xlabel= "tiempo [s]", ylabel = "Amplitud", title = "Señal muestreada con duración reducida 10%")
fig1.savefig("dur_90",dpi=300)




psola_dur_1_2 = psola_dur(data,pts_sonoros,porc = 0.8)
new_t_1_2 = np.arange(len(psola_dur_1_2))/16e3


fig2, ax2 = plt.subplots()
ax2.plot(new_t_1_2,psola_dur_1_2, label="data1")

ax2.set(xlabel= "tiempo [s]", ylabel = "Amplitud", title = "Señal muestreada con duración reducida 20%")
fig2.savefig("dur_80",dpi=300)
 
 
 
psola_dur_1_3 = psola_dur(data,pts_sonoros,porc = 0.7)
new_t_1_3 = np.arange(len(psola_dur_1_3))/16e3

 
fig3, ax3 = plt.subplots()
ax3.plot(new_t_1_3,psola_dur_1_3, label="data1")
 
ax3.set(xlabel= "tiempo [s]", ylabel = "Amplitud", title = "Señal muestreada con duración reducida 30%")
fig3.savefig("dur_70",dpi=300)
 
 
 














# =============================================================================
# 
# 
# 
# wavFile = "hh15.WAV"
# 
# sampleRate, data  = wfile.read(wavFile)
# t = np.arange(len(data))/sampleRate
#     
# Psola_array = np.zeros(len(data))
# Psola_array[t<0.5838] = data[t<0.5838]
# 
# for idx, val in enumerate(pts_sonoros[0:len(pts_sonoros)-1]):
#     Psola_array[(t>pts_sonoros[idx][1]) & (t<pts_sonoros[idx+1][0]) ] = data[(t>pts_sonoros[idx][1]) & (t<pts_sonoros[idx+1][0])]
#     #print(idx, val[1])
# 
# Psola_array[t>3.0986] = data[t>3.0986]
# 
# 
# hamm = sg.get_window("hamming",80)
# 
# aux_array =np.zeros(80)
# 
# #t_max_aux = 0
# max_aux =0
# 
# #Variable para indexar los periódos a multiplicar por hamming 
# val_aux = 0
# 
# #Variable para indexar desde donde in sumando los valores que son la multiplicación de una ventana hamming por los períodos
# #new_val_aux = 0
# 
# t_fund = 0.005
# n_fund = int(t_fund*16e3)
# 
# ########################################3
# 
# 
# for idx,val in enumerate(pts_sonoros):
#     val_aux = val[0]        
#     while(val_aux < val[1] + t_fund/2 ):
#         max_aux = np.argmax(data[  (t > val_aux) & (t < val_aux + t_fund)])
#         max_aux += int(val_aux*16e3)  
#         aux_array =np.multiply(data[max_aux - int(n_fund/2): max_aux + int(n_fund/2) ],hamm) 
#         #aux =  np.round(np.round(max_aux/16e3,4) - new_val_aux, 4) # Calculo la distancia al maximo para saber desde donde empezar a sumar
#         #Psola_array[  (t >= val_aux) & (t < val_aux + t_fund) ] =  aux_array
#         #Psola_array[  (t >= np.round(val_aux - aux,4) ) & (t <  np.round( new_val_aux - aux, 4 ) + (t_fund) ) ] +=  aux_array
#         Psola_array[ max_aux - int(n_fund/2) : max_aux + int(n_fund/2)  ] +=  aux_array
#         val_aux+=t_fund
#         val_aux = np.round(val_aux,4)
# for idx,val in enumerate(pts_sonoros):
#     val_final = val[1]
#     prim_idx = np.arange( int(val[0]*16e3), int(val[0]*16e3) +n_fund)
#     while(val_final - val[0]) > 0.9*(val[1]- val[0]):
#         Psola_array = np.delete(Psola_array,prim_idx)
#         val_final -= t_fund
# 
# =============================================================================





