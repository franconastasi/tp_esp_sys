#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 10 17:23:12 2018

@author: franco
"""

import scipy.io.wavfile as wfile

import matplotlib.pyplot as plt

import numpy as np



P0 = 1 # Amplitud = 1
F0 = 200 # frecuencia del pulso glótico

Fs = 16e3; # frecuencia de muestreo

Tp = 0.4/F0
Tn = 0.16/F0


#Genero un vector para el tiempo con un paso igual a 1/Fs y que llega hasta (1/F0 menos 1/Fs)
t = np.arange(0,1/F0,1/Fs)  # Genero la secuencia de tiempo como si muestreara a 16kHz

#Genero un vector de ceros con una longitud igual a la cantidad de príodos de muestreo que entran
#en un período del pulso glótico. 
Pg = np.zeros(round(Fs/F0))

#Genero la parte de apertura del pulso
Pg[0:round(Tp*Fs)] = P0*(1 - np.cos( np.pi * t[0:round(Tp*Fs)] / Tp  ))/2

#Genero la parte de cierre del pulso
Pg[round(Tp*Fs): round(Tp*Fs) + round(Tn*Fs)] = P0*np.cos( np.pi * ( t[round(Tp*Fs): round(Tp*Fs) + round(Tn*Fs) ] - Tp )/ (2*Tn) )


fig, ax = plt.subplots()
ax.plot(t*1e3, Pg, label="data")
ax.set(xlabel="Tiempo [ms]", ylabel= "Amplitud", title="Pulso glótico")
fig.savefig("pulso_glot",dpi=300)

#Genero un tren de 10 deltas cada Fs/F0 
PDelta = np.zeros(round(Fs/F0)*9 +1)
PDelta[0::80]= 1


#Hago la convolución con el pulso glótico
Pg_10_pulsos = np.convolve(Pg,PDelta)
#plt.plot(Pg_10_pulsos)


#Gráfico
t_10_pulsos = np.arange(0,10/F0,1/Fs)

fig1, ax1 = plt.subplots()
ax1.plot(t_10_pulsos*1e3, Pg_10_pulsos, label="data2")
ax1.set(xlabel="Tiempo [ms]", ylabel= "Amplitud", title="Pulso glótico convolucionado con 10 deltas")
fig1.savefig("tren_pulso_glot",dpi=300)

#Calculo las fft de un pulso y del tren de pulsos con el doble de puntos

#pulso_fft = np.fft.fft(Pg)
pulso_fft = np.fft.fft(Pg,1*len(Pg))
#tren_pulsos_fft = np.fft.fft(Pg_10_pulsos)
tren_pulsos_fft = np.fft.fft(Pg_10_pulsos, 1*len(Pg_10_pulsos))


#Gráfico
freq_1_pulso = np.arange(0,Fs,Fs/(1*len(Pg)))

fig2, ax2 = plt.subplots()
ax2.plot(freq_1_pulso,abs(pulso_fft), label="dat32")
ax2.set(title= "Espectro de 1 pulso glótico")
fig2.savefig("espect_pulso_glot",dpi=300)

freq_tren = np. arange(0,Fs,Fs/( 1*len(Pg_10_pulsos)))
fig3, ax3 = plt.subplots()
ax3.plot(freq_tren,abs(tren_pulsos_fft), label="dat32")
ax3.set(title= "Espectro de un tren pulsos glóticos")
fig3.savefig("espect_tren_pulso_glot",dpi=300)

#Grafico
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.fft.fftfreq.html#numpy.fft.fftfreq
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.fft.fft.html#numpy.fft.fft

## DE ACA PARA ABAJO NO SIRVE

#n = pulso_fft.size
#timestep = 0.1
#freq = np.fft.fftfreq(n, d=timestep)

#fig2, ax2 = plt.subplots()
#ax2.stem(freq, pulso_fft, label="dat32")
#ax2.set(title="Espectro de 1 pulso glótico")

#n3 = tren_pulsos_fft.size
#timestep3 = 0.1
#freq3 = np.fft.fftfreq(n3, d=timestep3)

#fig3, ax3 = plt.subplots()
#ax3.stem(freq3, tren_pulsos_fft, label="dat32")
#ax3.set(title="Espectro de un tren de pulsos glóticos")