#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 20:08:32 2018

@author: franco
"""

import matplotlib.pyplot as plt

from scipy import signal as sg

import numpy as np

import scipy.io.wavfile as wfile

from cepstrum import cepstrum



Fs = 16e3


## EJECUTAR EJ8 PARA QUE FUNCIONE

#Aplico la transformada cepstrum a cada fono, extendido a 30 ms 


fono_a_sint_50ms =abs( np.transpose(fono_a_sint) )

cepstrum_fono_a_sint= cepstrum(fono_a_sint_50ms)
t_fono_a = np.arange(len(cepstrum_fono_a_sint))/Fs

fig_a, ax_a = plt.subplots(2,1)
ax_a[0].plot(t_fono_a*1e3,cepstrum_fono_a_sint)
ax_a[0].set(xlabel="Tiempo [ms]", ylabel="Parte real de \n la transformada cepstrum",title = "Parte real de la transformada cepstrum - fono [a] sintetizado")
ax_a[0].grid()

ax_a[1].plot(t_fono_a[(t_fono_a<0.02) & (t_fono_a>0.002) ]*1e3,cepstrum_fono_a_sint[ (t_fono_a<0.02) & (t_fono_a>0.002)  ])
ax_a[1].set(xlabel="Tiempo {ms]", ylabel="Parte real de\n la transformada cepstrum")
ax_a[1].grid()

fig_a.tight_layout()
fig_a.savefig("cepstrum_sint_a",dpi=300)



## Estimo la respuesta en frecuencia.

est_resp_freq = cepstrum_fono_a_sint
est_resp_freq[ (t_fono_a >0.0049) ] = 0
#est_resp_freq[ 120:800 ] = 0
est_resp_freq = np.fft.fft(est_resp_freq)
est_resp_freq = 10**est_resp_freq


fig_est_rf ,ax_est_rf = plt.subplots()
ax_est_rf.set(xlabel="Frecuencia", ylabel= "Amplitud", Title= "Estimación de la respuesta en frecuencia")
ax_est_rf.plot(np.arange(0,2*np.pi,2*np.pi/len(Pg_10_pulsos)), est_resp_freq)
fig_est_rf.savefig("est_resp_frec_a",dpi = 300)
###########
##########

fono_e_sint_50ms =abs( np.transpose(fono_e_sint ))# le aplico el módulo porque son valores complejos con parte imaginario de aprox e-14

cepstrum_fono_e_sint= cepstrum(fono_e_sint_50ms)
t_fono_e = np.arange(len(cepstrum_fono_e_sint))/Fs

fig_e, ax_e = plt.subplots(2, 1)
ax_e[0].plot(t_fono_e*1e3,cepstrum_fono_e_sint)
ax_e[0].set(xlabel="Tiempo [ms]", ylabel="Parte real de \n la transformada cepstrum", title = "Parte real de la transformada cepstrum - fono [e] sintetizado")
ax_e[0].grid()

ax_e[1].plot(t_fono_e[(t_fono_e<0.02) & (t_fono_e>0.002) ]*1e3,cepstrum_fono_e_sint[ (t_fono_e<0.02) & (t_fono_e>0.002)  ])
ax_e[1].set(xlabel="Tiempo [ms]", ylabel="Parte real de\n la transformada cepstrum")
ax_e[1].grid()

fig_e.tight_layout()
fig_e.savefig("cepstrum_sint_e",dpi=300)


###########
##########


fono_i_sint_50ms =abs( np.transpose(fono_i_sint ))# le aplico el módulo porque son valores complejos con parte imaginario de aprox e-14

cepstrum_fono_i_sint= cepstrum(fono_i_sint_50ms)
t_fono_i = np.arange(len(cepstrum_fono_i_sint))/Fs

fig_i, ax_i = plt.subplots(2, 1)
ax_i[0].plot(t_fono_i*1e3,cepstrum_fono_i_sint)
ax_i[0].set(xlabel="Tiempo [ms]", ylabel="Parte real de \n la transformada cepstrum", title = "Parte real de la transformada cepstrum - fono [i] sintetizado")
ax_i[0].grid()

ax_i[1].plot(t_fono_i[(t_fono_i<0.02) & (t_fono_i>0.002) ]*1e3,cepstrum_fono_i_sint[ (t_fono_i<0.02) & (t_fono_i>0.002)  ])
ax_i[1].set(xlabel="Tiempo [ms]", ylabel="Parte real de\n la transformada cepstrum")
ax_i[1].grid()

fig_i.tight_layout()
fig_i.savefig("cepstrum_sint_i",dpi=300)


###########
##########

fono_o_sint_50ms =abs( np.transpose(fono_o_sint ))# le aplico el módulo porque son valores complejos con parte imaginario de aprox e-14

cepstrum_fono_o_sint= cepstrum(fono_o_sint_50ms)
t_fono_o = np.arange(len(cepstrum_fono_o_sint))/Fs

fig_o, ax_o = plt.subplots(2, 1)
ax_o[0].plot(t_fono_o*1e3,cepstrum_fono_o_sint)
ax_o[0].set(xlabel="Tiempo [ms]", ylabel="Parte real de \n la transformada cepstrum", title = "Parte real de la transformada cepstrum - fono [o] sintetizado")
ax_o[0].grid()

ax_o[1].plot(t_fono_o[(t_fono_o<0.02) & (t_fono_o>0.002) ]*1e3,cepstrum_fono_o_sint[ (t_fono_o<0.02) & (t_fono_o>0.002)  ])
ax_o[1].set(xlabel="Tiempo [ms]", ylabel="Parte real de\n la transformada cepstrum")
ax_o[1].grid()

fig_o.tight_layout()
fig_o.savefig("cepstrum_sint_o",dpi=300)


###########
##########

fono_u_sint_50ms =abs( np.transpose(fono_u_sint ))# le aplico el módulo porque son valores complejos con parte imaginario de aprox e-14

cepstrum_fono_u_sint= cepstrum(fono_u_sint_50ms)
t_fono_u = np.arange(len(cepstrum_fono_u_sint))/Fs

fig_u, ax_u = plt.subplots(2, 1)
ax_u[0].plot(t_fono_u*1e3,cepstrum_fono_u_sint)
ax_u[0].set(xlabel="Tiempo [ms]", ylabel="Parte real de \n la transformada cepstrum", title = "Parte real de la transformada cepstrum - fono [u] sintetizado")
ax_u[0].grid()

ax_u[1].plot(t_fono_u[(t_fono_u<0.02) & (t_fono_u>0.002) ]*1e3,cepstrum_fono_u_sint[ (t_fono_u<0.02) & (t_fono_u>0.002)  ])
ax_u[1].set(xlabel="Tiempo [ms]", ylabel="Parte real de\n la transformada cepstrum")
ax_u[1].grid()

fig_u.tight_layout()
fig_u.savefig("cepstrum_sint_u",dpi=300)









