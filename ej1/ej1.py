# -*- coding: utf-8 -*-
"""
Created on Thu Jun  7 10:53:33 2018

@author: franco
"""
import scipy.io.wavfile as wfile

import matplotlib.pyplot as plt

import numpy as np

wavFile = "hh15.WAV"

sampleRate, data  = wfile.read(wavFile)
t = np.arange(len(data))/sampleRate


fig, ax = plt.subplots()
ax.plot(t, data, label="data")

ax.set(xlabel= "tiempo [s]", ylabel = "Amplitud", title = "Señal muestreada")

plt.text(0.54,20431, "C", fontsize=6)
plt.text(0.62,20431, "O", fontsize=6)
plt.text(0.75,20431, "N", fontsize=6)
plt.text(0.92,20431, "T", fontsize=6)
plt.text(0.96,20431, "A", fontsize=6)
plt.text(1.02,20431, "G", fontsize=6)
plt.text(1.12,20431, "I", fontsize=6)
plt.text(1.24,20431, "A", fontsize=6)


plt.text(1.35,20431, "E", fontsize=6)
plt.text(1.51,20431, "N", fontsize=6)
plt.text(1.60,20431, "E", fontsize=6)
plt.text(1.72,20431, "R", fontsize=6)
plt.text(1.83,20431, "G", fontsize=6)
plt.text(1.97,20431, "I", fontsize=6)
plt.text(2.11,20431, "A", fontsize=6)

plt.text(2.20,20431, "Y", fontsize=6)

plt.text(2.35,20431, "F", fontsize=6)
plt.text(2.50,20431, "E", fontsize=6)
plt.text(2.65,20431, "R", fontsize=6)
plt.text(2.74,20431, "V", fontsize=6)
plt.text(2.85,20431, "O", fontsize=6)
plt.text(3.04,20431, "R", fontsize=6)



fig.savefig("cont_en_fer",dpi=300)

