#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  8 11:16:51 2018

@author: franco
"""


import scipy.io.wavfile as wfile

import matplotlib.pyplot as plt

import numpy as np

wavFile = "hh15.WAV"

sampleRate, data  = wfile.read(wavFile)
t = np.arange(len(data))/sampleRate

# Obtengo los coeficientes de Fourier como en el ejercicio 2


#Defino los tiempos iniciales y finales para 1 períodos y múltiples (5) períodos 
#basado en el gráfico del ejercicio 1

t_init_a_1_per = 0.966672
t_fin_a_1_per  = 0.97151



t_init_a_mul_per = 0.966672
t_fin_a_mul_per = 0.985419


#obtengo los arreglos correspondientes a 1 período y a múltiples del fono []

a_1_per = data[ (t> t_init_a_1_per) & (t< t_fin_a_1_per)]
a_1_per_lenght = len(a_1_per)

a_mul_per = data[ (t> t_init_a_mul_per) & (t< t_fin_a_mul_per)]
a_mul_per_lenght = len(a_mul_per)


#Calculo los coeficientes de fourier 

a_1_per_fft = np.fft.fft(a_1_per)/a_1_per_lenght

a_mul_per_fft = np.fft.fft(a_mul_per)/a_mul_per_lenght


# Calculo la transformada discreta de Fourier inversa  (reconstruida == recon)


a_1_per_recon = np.fft.ifft(a_1_per_fft)

a_mul_per_recon = np.fft.ifft(a_mul_per_fft)

#Gráfico la totalidad de los puntos muestreados

t_recon_1_per = np.arange(len(a_1_per_recon)) * 1000/sampleRate #ms

t_recon_mul_per = np.arange(len(a_mul_per_recon))*1000/sampleRate #ms


fig1, ax1 =plt.subplots()
ax1.set(xlabel = "tiempo [ms]", ylabel = "Amplitud", title = "Fono [a] reconstruido - 1 período muestreado")
ax1.plot(t_recon_1_per,a_1_per_recon)


fig2, ax2 =plt.subplots()
ax2.set(xlabel = "tiempo [ms]", ylabel = "Amplitud", title = "Fono [a] reconstruido - 5 períodos muestreados")
ax2.plot(t_recon_mul_per,a_mul_per_recon)





#Gráfico el fono a reconstruido a partir de los primeros puntos arrojados por la
#FFT -> primeros 30 de 80 para el fono a muestreado con 1 período y primeros 100
#de 300 para el fono a muestreado con 5 períodos.



#a_1_per_recon_primeras_30 = np.fft.ifft(a_1_per_fft[0:29])
#t_recon_1_per_prim_30 = 1000*np.arange(len(a_1_per_recon_primeras_30))/sampleRate #ms

# Pruebas 
a_1_per_fft_primeras_30 = np.zeros(78)
a_1_per_fft_primeras_30[0:29] = a_1_per_fft[0:29]
a_1_per_recon_primeras_30 = np.fft.ifft(a_1_per_fft_primeras_30)
t_recon_1_per_prim_30 = 1000*np.arange(len(a_1_per_recon_primeras_30))/sampleRate #ms




fig3, ax3 =plt.subplots()
fig3.set_size_inches(9.0, 5.0)
ax3.set(xlabel = "Tiempo [ms]", ylabel = "Amplitud",
        title = "Fono [a] reconstruido - 1 período muestreado\nComparación: Señal original vs. Señal reconstruida utilizando primeras 30 frecuencias")
ax3.plot(t_recon_1_per,a_1_per_recon, 'k', label="Señal original reconstrida")
ax3.plot(t_recon_1_per_prim_30, a_1_per_recon_primeras_30, 'b', label="Señal reconstruida - primeras 30 muestras")
ax3.legend()
fig3.tight_layout()
fig3.savefig("ifft_1_per_prim_30", dpi=300)



#a_mul_per_recon_primeras_100 = np.fft.ifft(a_mul_per_fft[0:100])
#t_recon_1_per_prim_100 =1000* np.arange(len(a_mul_per_recon_primeras_100))/sampleRate #ms


#Pruebas
a_mul_per_fft_primeras_100 = np.zeros(300)
a_mul_per_fft_primeras_100[0:100] = a_mul_per_fft[0:100]
a_mul_per_recon_primeras_100 = np.fft.ifft(a_mul_per_fft_primeras_100)
t_recon_1_per_prim_100 = 1000*np.arange(len(a_mul_per_recon_primeras_100))/sampleRate #ms



fig4, ax4 =plt.subplots()
fig4.set_size_inches(12.0, 6.0)
ax4.set(xlabel = "Tiempo [ms]", ylabel = "Amplitud",
        title = "Fono [a] reconstruido - 5 períodos muestreados \n Comparación: Señal original vs. Señal reconstruida utilizando primeras 100 frecuencias")
ax4.plot(t_recon_mul_per,a_mul_per_recon,'k',
         label="Señal original reconstruida")

ax4.plot(t_recon_1_per_prim_100, a_mul_per_recon_primeras_100, 'b',
         label="Señal reconstruida - primeras 100 muestras")
ax4.legend()

fig4.savefig("ifft_mul_per_prim_100", dpi=300)



#Gráfico el fono a reconstruido a partir de los valores realesarrojados por la
#FFT 

a_1_per_recon_real = np.fft.ifft(a_1_per_fft.real)

a_mul_per_recon_real = np.fft.ifft(a_mul_per_fft.real)

fig5, ax5 =plt.subplots()
ax5.set(xlabel = "Tiempo [ms]", ylabel = "Amplitud", title = "Fono [a] reconstruido - 1 período muestreado\n Parte real de lo obtenido con la FFT")
ax5.plot(t_recon_1_per,a_1_per_recon_real)


fig6, ax6 =plt.subplots()
ax6.set(xlabel = "tiempo [ms]", ylabel = "Amplitud", title = "Fono [a] reconstruido - 5 períodos muestreados \n Parte real de lo obtenido con la FFT")
ax6.plot(t_recon_mul_per,a_mul_per_recon_real)



#Gráfico el fono a reconstruido a partir de la parte imaginaria de los valores arrojados por la
#FFT 

a_1_per_recon_imag = np.fft.ifft(a_1_per_fft.imag)

a_mul_per_recon_imag = np.fft.ifft(a_mul_per_fft.imag)

fig7, ax7 =plt.subplots()
ax7.set(xlabel = "tiempo [ms]",
        ylabel = "Amplitud",
        title = "Fono [a] reconstruido - 1 período muestreado\n Parte imaginaria de lo obtenido con la FFT")
ax7.plot(t_recon_1_per,a_1_per_recon_imag)


fig8, ax8 =plt.subplots()
ax8.set(xlabel = "tiempo [ms]",
        ylabel = "Amplitud",
        title = "Fono [a] reconstruido - 5 períodos muestreados \n Parte imaginaria de lo obtenido con la FFT")
ax8.plot(t_recon_mul_per,a_mul_per_recon_imag)



#Gráfico el fono a reconstruido a partir del módulo de los valores arrojados por la
#FFT 

a_1_per_recon_abs = np.fft.ifft(abs(a_1_per_fft))

a_mul_per_recon_abs = np.fft.ifft(abs(a_mul_per_fft))

fig9, ax9 =plt.subplots()
ax9.set(xlabel = "tiempo [ms]",
        ylabel = "Amplitud",
        title = "Fono [a] reconstruido - 1 período muestreado\n Valor absoluto de lo obtenido con la FFT")
ax9.plot(t_recon_1_per,a_1_per_recon_abs)


fig10, ax10 =plt.subplots()
ax10.set(xlabel = "tiempo [ms]",
        ylabel = "Amplitud",
        title = "Fono [a] reconstruido - 5 períodos muestreados \n Valor absoluto de lo obtenido con la FFT")
ax10.plot(t_recon_mul_per,a_mul_per_recon_abs)



# Se hace la comparación entre los gráficos


fig, ax = plt.subplots()
ax.set(xlabel = "tiempo [ms]",
       ylabel = "Amplitud",
       title = "Comparación del Fono [a] reconstruido - 1 período muestreado")
ax.plot(t_recon_1_per,a_1_per_recon,'k', label= "señal reconstruida")
ax.plot(t_recon_1_per,a_1_per_recon_real,'r', label= "señal reconstruida con parte real")
ax.plot(t_recon_1_per,a_1_per_recon_imag,'b', label= "señal reconstruida con parte imaginaria")
ax.plot(t_recon_1_per,a_1_per_recon_abs,'c', label= "señal reconstruida con valor absoluto")

ax.legend()

fig.savefig("comp_fono_a_1_per", dpi = 300)

figg, axx = plt.subplots()
axx.set(xlabel = "tiempo [ms]",
       ylabel = "Amplitud",
       title = "Comparación del Fono [a] reconstruido - 5 períodos muestreados")
axx.plot(t_recon_mul_per,a_mul_per_recon,'k', label= "señal reconstruida")
axx.plot(t_recon_mul_per,a_mul_per_recon_real,'r', label= "señal reconstruida con parte real")
axx.plot(t_recon_mul_per,a_mul_per_recon_imag,'b', label= "señal reconstruida con parte imaginaria")
axx.plot(t_recon_mul_per,a_mul_per_recon_abs,'c', label= "señal reconstruida con valor absoluto")

axx.legend()

figg.savefig("comp_fono_a_mul_per", dpi = 300)

