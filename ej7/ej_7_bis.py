#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 10 19:52:26 2018

@author: franco
"""

## IGUAL QUE EL EJERCICIO 7 PERO CON SISTEMA DISCRETO; USAR ESTE

import matplotlib.pyplot as plt

from scipy import signal as sg

import numpy as np

Fs = 16e3


#Defino 2 matrices para los valores de F y B
F = np.matrix('830 1400 2890 3930; 500 2000 3130 4150; 330 2765 3740 4366; 546 934 2966 3930; 382 740 2760 3380')

B = np.matrix('110 160 210 230; 80 156 190 220; 70 130 178 200; 97 130 185 240; 74 150 210 180')

#En cada uno de los siguientes arrays guardo el valor p_n de cada vocal

a = (np.e)**( (2*np.pi) * (-np.array(B)[0]  + np.array(F)[0]*1j )/Fs ) 
e = (np.e)**( (2*np.pi) * (-np.array(B)[1]  + np.array(F)[1]*1j )/Fs ) 
i = (np.e)**( (2*np.pi) * (-np.array(B)[2]  + np.array(F)[2]*1j )/Fs ) 
o = (np.e)**( (2*np.pi) * (-np.array(B)[3]  + np.array(F)[3]*1j )/Fs ) 
u = (np.e)**( (2*np.pi) * (-np.array(B)[4]  + np.array(F)[4]*1j )/Fs ) 



#Grafico diagrama de ceros y polos para 'a'
fig_a, ax_a = plt.subplots()
ax_a.plot(0,0,'or')
ax_a.plot(np.real(a[0]),np.imag(a[0]),'xb')
ax_a.plot(np.real(a[1]),np.imag(a[1]),'xb')
ax_a.plot(np.real(a[2]),np.imag(a[2]),'xb')
ax_a.plot(np.real(a[3]),np.imag(a[3]),'xb')
ax_a.plot(np.real(a[0]),-np.imag(a[0]),'xb')
ax_a.plot(np.real(a[1]),-np.imag(a[1]),'xb')
ax_a.plot(np.real(a[2]),-np.imag(a[2]),'xb')
ax_a.plot(np.real(a[3]),-np.imag(a[3]),'xb')
ax_a.legend(['Ceros', 'Polos'])
ax_a.set(xlabel= "Eje real", ylabel = "Eje imaginario", title = "Diagrama de polos y ceros - fono [a]")
ax_a.grid()
#plt.show()
fig_a.savefig("diag_zp_a", dpi =300)



#Grafico diagrama de ceros y polos para 'e'
fig_e, ax_e = plt.subplots()
ax_e.plot(0,0,'or')
ax_e.plot(np.real(e[0]),np.imag(e[0]),'xb')
ax_e.plot(np.real(e[1]),np.imag(e[1]),'xb')
ax_e.plot(np.real(e[2]),np.imag(e[2]),'xb')
ax_e.plot(np.real(e[3]),np.imag(e[3]),'xb')
ax_e.plot(np.real(e[0]),-np.imag(e[0]),'xb')
ax_e.plot(np.real(e[1]),-np.imag(e[1]),'xb')
ax_e.plot(np.real(e[2]),-np.imag(e[2]),'xb')
ax_e.plot(np.real(e[3]),-np.imag(e[3]),'xb')
ax_e.legend(['Ceros', 'Polos'])
ax_e.set(xlabel= "Eje real", ylabel = "Eje imaginario", title = "Diagrama de polos y ceros - fono [e]")
ax_e.grid()
#plt.show()
fig_e.savefig("diag_zp_e", dpi =300)




#Grafico diagrama de ceros y polos para 'i'
fig_i, ax_i = plt.subplots()
ax_i.plot(0,0,'or')
ax_i.plot(np.real(i[0]),np.imag(i[0]),'xb')
ax_i.plot(np.real(i[1]),np.imag(i[1]),'xb')
ax_i.plot(np.real(i[2]),np.imag(i[2]),'xb')
ax_i.plot(np.real(i[3]),np.imag(i[3]),'xb')
ax_i.plot(np.real(i[0]),-np.imag(i[0]),'xb')
ax_i.plot(np.real(i[1]),-np.imag(i[1]),'xb')
ax_i.plot(np.real(i[2]),-np.imag(i[2]),'xb')
ax_i.plot(np.real(i[3]),-np.imag(i[3]),'xb')
ax_i.legend(['Ceros', 'Polos'])
ax_i.set(xlabel= "Eje real", ylabel = "Eje imaginario", title = "Diagrama de polos y ceros - fono [i]")
ax_i.grid()
plt.show()
fig_i.savefig("diag_zp_i", dpi =300)




#Grafico diagrama de ceros y polos para 'o'
fig_o, ax_o = plt.subplots()
ax_o.plot(0,0,'or')
ax_o.plot(np.real(o[0]),np.imag(o[0]),'xb')
ax_o.plot(np.real(o[1]),np.imag(o[1]),'xb')
ax_o.plot(np.real(o[2]),np.imag(o[2]),'xb')
ax_o.plot(np.real(o[3]),np.imag(o[3]),'xb')
ax_o.plot(np.real(o[0]),-np.imag(o[0]),'xb')
ax_o.plot(np.real(o[1]),-np.imag(o[1]),'xb')
ax_o.plot(np.real(o[2]),-np.imag(o[2]),'xb')
ax_o.plot(np.real(o[3]),-np.imag(o[3]),'xb')
ax_o.legend(['Ceros', 'Polos'])
ax_o.set(xlabel= "Eje real", ylabel = "Eje imaginario", title = "Diagrama de polos y ceros - fono [o]")
ax_o.grid()
plt.show()
fig_o.savefig("diag_zp_o", dpi =300)




#Grafico diagrama de ceros y polos para 'u'
fig_u, ax_u = plt.subplots()
ax_u.plot(0,0,'or')
ax_u.plot(np.real(u[0]),np.imag(u[0]),'xb')
ax_u.plot(np.real(u[1]),np.imag(u[1]),'xb')
ax_u.plot(np.real(u[2]),np.imag(u[2]),'xb')
ax_u.plot(np.real(u[3]),np.imag(u[3]),'xb')
ax_u.plot(np.real(u[0]),-np.imag(u[0]),'xb')
ax_u.plot(np.real(u[1]),-np.imag(u[1]),'xb')
ax_u.plot(np.real(u[2]),-np.imag(u[2]),'xb')
ax_u.plot(np.real(u[3]),-np.imag(u[3]),'xb')
ax_u.legend(['Ceros', 'Polos'])
ax_u.set(xlabel= "Eje real", ylabel = "Eje imaginario", title = "Diagrama de polos y ceros - fono [u]")
ax_u.grid()
plt.show()
fig_u.savefig("diag_zp_u", dpi =300)


#Genero una transferencia para el fono a con los polos p_i, p*_i, teniendo en cuenta que la ganancia es 1 y en el numerador hay un z^8
H_a = sg.dlti([0,0,0,0,0,0,0,0], [a[0], np.conjugate(a[0]), a[1], np.conjugate(a[1]), a[2], np.conjugate(a[2]), a[3], np.conjugate(a[3]) ], 1)
#Calculo la respuesta al impulso y grafico
#fig_resp_imp_a, ax_resp_imp_a = plt.subplots()
#ax_resp_imp_a.plot(sg.impulse(H_a)[0] , sg.impulse(H_a)[1])
#ax_resp_imp_a.set(xlabel="Tiempo [s]", ylabel= "Amplitud", title = "Respuesta al impulso - fono a")
#ax_resp_imp_a.grid()
#fig_resp_imp_a.savefig("resp_imp_a", dpi = 300)

H_a_resp_freq = H_a.freqresp(w = np.arange(0,2*np.pi,2*np.pi/len(Pg_10_pulsos)))



aux_a= sg.dimpulse(H_a, t = np.arange(0,2*np.pi,2*np.pi/len(Pg_10_pulsos)))
#plt.plot(H_a_resp_freq[1])
#plt.plot(np.fft.ifft(H_a_resp_freq[1]))
#plt.plot(sg.dfreqresp(H_a, w = np.arange(0,2*np.pi,2*np.pi/len(Pg_10_pulsos))))

#sg.fft.ifft()

fig_resp_freq_a, ax_resp_freq_a = plt.subplots()
ax_resp_freq_a.plot(H_a_resp_freq[0],abs(H_a_resp_freq[1]))
ax_resp_freq_a.set(xlabel="Frecuencia [rad/s]", ylabel= "Módulo de la respuesta en frecuencia", title= "Respuesta en frecuencia del fono [e]")
ax_resp_freq_a.grid()
fig_resp_freq_a.savefig("resp_freq_a", dpi = 300)

#Genero una transferencia para el fono e con los polos p_i, p*_i, teniendo en cuenta que la ganancia es 1 y en el numerador hay un z^8
H_e = sg.dlti([0,0,0,0,0,0,0,0], [e[0], np.conjugate(e[0]), e[1], np.conjugate(e[1]), e[2], np.conjugate(e[2]), e[3], np.conjugate(e[3]) ], 1)
#Calculo la respuesta al impulso y grafico
#fig_resp_imp_e, ax_resp_imp_e = plt.subplots()
#ax_resp_imp_e.plot(sg.impulse(H_e)[0] , sg.impulse(H_e)[1])
#ax_resp_imp_e.set(xlabel="Tiempo [s]", ylabel= "Amplitud", title = "Respuesta al impulso - fono e")
#ax_resp_imp_e.grid()
#fig_resp_imp_e.savefig("resp_imp_e", dpi = 300)

H_e_resp_freq = H_e.freqresp(w = np.arange(0,2*np.pi,2*np.pi/len(Pg_10_pulsos)))
fig_resp_freq_e, ax_resp_freq_e = plt.subplots()
ax_resp_freq_e.plot(H_e_resp_freq[0],abs(H_e_resp_freq[1]))
ax_resp_freq_e.set(xlabel="Frecuencia [rad/s]", ylabel= "Módulo de la respuesta en frecuencia", title= "Respuesta en frecuencia del fono [a]")
ax_resp_freq_e.grid()
fig_resp_freq_e.savefig("resp_freq_e", dpi = 300)


#plt.plot(np.fft.ifft(H_e_resp_freq[1]))

#Genero una transferencia para el fono i con los polos p_i, p*_i, teniendo en cuenta que la ganancia es 1 y en el numerador hay un z^8
H_i = sg.dlti([0,0,0,0,0,0,0,0], [i[0], np.conjugate(i[0]), i[1], np.conjugate(i[1]), i[2], np.conjugate(i[2]), i[3], np.conjugate(i[3]) ], 1)
#Calculo la respuesta al impulso y grafico
#fig_resp_imp_i, ax_resp_imp_i = plt.subplots()
#ax_resp_imp_i.plot(sg.impulse(H_i)[0] , sg.impulse(H_i)[1])
#ax_resp_imp_i.set(xlabel="Tiempo [s]", ylabel= "Amplitud", title = "Respuesta al impulso - fono i")
#ax_resp_imp_i.grid()
#fig_resp_imp_i.savefig("resp_imp_i", dpi = 300)

H_i_resp_freq = H_i.freqresp(w = np.arange(0,2*np.pi,2*np.pi/len(Pg_10_pulsos)))
fig_resp_freq_i, ax_resp_freq_i = plt.subplots()
ax_resp_freq_i.plot(H_i_resp_freq[0],abs(H_i_resp_freq[1]))
ax_resp_freq_i.set(xlabel="Frecuencia [rad/s]", ylabel= "Módulo de la respuesta en frecuencia", title= "Respuesta en frecuencia del fono [i]")
ax_resp_freq_i.grid()
fig_resp_freq_i.savefig("resp_freq_i", dpi = 300)



#plt.plot(np.fft.ifft(H_i_resp_freq[1]))

#Genero una transferencia para el fono o con los polos p_i, p*_i, teniendo en cuenta que la ganancia es 1 y en el numerador hay un z^8
H_o = sg.dlti([0,0,0,0,0,0,0,0], [o[0], np.conjugate(o[0]), o[1], np.conjugate(o[1]), o[2], np.conjugate(o[2]), o[3], np.conjugate(o[3]) ], 1)
#Calculo la respuesta al impulso y grafico
#fig_resp_imp_o, ax_resp_imp_o = plt.subplots()
#ax_resp_imp_o.plot(sg.impulse(H_o)[0] , sg.impulse(H_o)[1])
#ax_resp_imp_o.set(xlabel="Tiempo [s]", ylabel= "Amplitud", title = "Respuesta al impulso - fono o")
#ax_resp_imp_o.grid()
#fig_resp_imp_o.savefig("resp_imp_o", dpi = 300)

H_o_resp_freq = H_o.freqresp(w = np.arange(0,2*np.pi,2*np.pi/len(Pg_10_pulsos)))
fig_resp_freq_o, ax_resp_freq_o = plt.subplots()
ax_resp_freq_o.plot(H_o_resp_freq[0],abs(H_o_resp_freq[1]))
ax_resp_freq_o.set(xlabel="Frecuencia [rad/s]", ylabel= "Módulo de la respuesta en frecuencia", title= "Respuesta en frecuencia del fono [o]")
ax_resp_freq_o.grid()
fig_resp_freq_o.savefig("resp_freq_o", dpi = 300)


#plt.plot(np.fft.ifft(H_o_resp_freq[1]))

#Genero una transferencia para el fono u con los polos p_i, p*_i, teniendo en cuenta que la ganancia es 1 y en el numerador hay un z^8
H_u = sg.dlti([0,0,0,0,0,0,0,0], [u[0], np.conjugate(u[0]), u[1], np.conjugate(u[1]), u[2], np.conjugate(u[2]), u[3], np.conjugate(u[3]) ], 1)
#Calculo la respuesta al impulso y grafico
#fig_resp_imp_u, ax_resp_imp_u = plt.subplots()
#ax_resp_imp_u.plot(sg.impulse(H_u)[0] , sg.impulse(H_u)[1])
#ax_resp_imp_u.set(xlabel="Tiempo [s]", ylabel= "Amplitud", title = "Respuesta al impulso - fono u")
#ax_resp_imp_u.grid()
#fig_resp_imp_u.savefig("resp_imp_u", dpi = 300)


H_u_resp_freq = H_u.freqresp(w = np.arange(0,2*np.pi,2*np.pi/len(Pg_10_pulsos)))
fig_resp_freq_u, ax_resp_freq_u = plt.subplots()
ax_resp_freq_u.plot(H_u_resp_freq[0],abs(H_u_resp_freq[1]))
ax_resp_freq_u.set(xlabel="Frecuencia [rad/s]", ylabel= "Módulo de la respuesta en frecuencia", title= "Respuesta en frecuencia del fono [u]")
ax_resp_freq_u.grid()
fig_resp_freq_u.savefig("resp_freq_u", dpi = 300)




fig_resp_imp,ax_resp_imp = plt.subplots()
ax_resp_imp.set(ylabel= "Amplitud", title="Transformada inversa discreta de Fourier \n para el fono [a] de la transferencia obtenida")
ax_resp_imp.plot(np.fft.ifft(H_a_resp_freq[1]))
fig_resp_imp.savefig("resp_imp_a", dpi=300)


fig_resp_imp,ax_resp_imp = plt.subplots()
ax_resp_imp.set(ylabel= "Amplitud", title="Transformada inversa discreta de Fourier \n para el fono [e] de la transferencia obtenida")
ax_resp_imp.plot(np.fft.ifft(H_e_resp_freq[1]))
fig_resp_imp.savefig("resp_imp_e", dpi=300)


fig_resp_imp,ax_resp_imp = plt.subplots()
ax_resp_imp.set(ylabel= "Amplitud", title="Transformada inversa discreta de Fourier \n para el fono [i] de la transferencia obtenida")
ax_resp_imp.plot(np.fft.ifft(H_i_resp_freq[1]))
fig_resp_imp.savefig("resp_imp_i", dpi=300)


fig_resp_imp,ax_resp_imp = plt.subplots()
ax_resp_imp.set(ylabel= "Amplitud", title="Transformada inversa discreta de Fourier \n para el fono [o] de la transferencia obtenida")
ax_resp_imp.plot(np.fft.ifft(H_o_resp_freq[1]))
fig_resp_imp.savefig("resp_imp_o", dpi=300)


fig_resp_imp,ax_resp_imp = plt.subplots()
ax_resp_imp.set(ylabel= "Amplitud", title="Transformada inversa discreta de Fourier \n para el fono [u] de la transferencia obtenida")
ax_resp_imp.plot(np.fft.ifft(H_u_resp_freq[1]))
fig_resp_imp.savefig("resp_imp_u", dpi=300)

#fig_resp_imp.tight_layout()

### PRUEBAS
#num = [0, 1]
#den = [1e-3, 1]

##trans = sg.TransferFunction(num, den)

#trans2 = sg.lti([],[-1e3],1e3)
#trans3 = sg.lti(num,den)
#plt.plot( np.log10( trans2.freqresp()[0]),20*np.log10 (trans2.freqresp()[1]))
#plt.plot(trans2.freqresp(w = np.arange(1,1e4,1e2))[0],trans2.freqresp(w = np.arange(1,1e4,1e2))[1])
#plt.plot(trans3.freqresp(w = np.arange(1,1e4,1e2))[0], np.abs( trans3.freqresp(w = np.arange(1,1e4,1e2))[1]))
#plt.plot(np.log10(trans3.freqresp(w = np.arange(1,1e4,1e2))[0]),20*np.log10(trans3.freqresp(w = np.arange(1,1e4,1e2))[1]))
#plt.plot(sg.impulse(trans2)[0],sg.impulse(trans2)[1])
#sign = sg.impulse(trans)



#plt.plot(sg.impulse(trans)[0],sg.impulse(trans)[1])
#plt.plot(sg.impulse(trans)[1])
#plt.show()

